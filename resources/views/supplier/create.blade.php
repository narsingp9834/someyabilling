@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
           
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="jquery-val-form" method="POST" action="{{url('supplier/save')}}" autocomplete="off">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">{{__("labels.add")}} Purchase</h5>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="">Date</label>
                                        <input type="text" name="date" id="date" class="form-control flatpickr" placeholder="DD-MM-YYYY"> 
                                    </div>
                                </div>


                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="for" class="">Invoice No.</label>
                                        <input type="text" class="form-control" name="invoice_no"
                                            placeholder="Enter Invoice No." autofocus required>
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="for" class="">Supplier Name</label>
                                        <input type="text" class="form-control" name="supplier_name"
                                            placeholder="Enter Supplier Name" required>
                                    </div>
                                </div>


                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="for" class="">Phone No</label>
                                        <input type="text" class="form-control" name="phone_no"
                                            placeholder="Enter Phone No." autofocus required>
                                    </div>

                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="for" class="">GST No</label>
                                        <input type="text" class="form-control" name="gst_no"
                                            placeholder="Enter Gst No" autofocus required>
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="for" class="">Product</label>
                                        <input type="text" class="form-control" name="product"
                                            placeholder="Enter Product Name" required>
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="for" class=""> HSN Code</label>
                                        <input type="text" class="form-control" name="hsn_code"
                                            placeholder="Enter Hsn No." autofocus required>
                                    </div>

                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="for" class="">Price</label>
                                        <input type="text" class="form-control" name="price" id="price"
                                            placeholder="Enter Price" autofocus required>
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="for" class="">Quantity</label>
                                        <input type="text" class="form-control" name="quantity" id="quantity"
                                            placeholder="Enter quantity">
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="for" class=""> IGST</label>
                                        <input type="text" class="form-control" name="igst" id="igst"
                                            placeholder="Enter IGST" autofocus required>
                                    </div>

                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="for" class="">CGST</label>
                                        <input type="text" class="form-control" name="cgst" id="cgst"
                                            placeholder="Enter CGST" autofocus required>
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label id="for" class="">SGST</label>
                                        <input type="text" class="form-control" name="sgst" id="sgst"
                                            placeholder="Enter SGST">
                                    </div>
                                </div>

                                
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Payment Mode</label>
                                        <select class="form-control" name="payment_mode">
                                            <option>Select Payment Mode</option>
                                            <option value="Pending">Pending</option>
                                            <option value="Paid">Paid</option>
                                        </select>

                                    </div>

                                </div>
                              
                                <div class="col-4">
                                    <div class="form-group">
                                    <label for="">State</label>
                                        <select class="form-control" name="state_id" id="state_id">
                                            <option value="">Select</option>
                                            @foreach ($states as $state)
                                                <option value="{{$state->state_id}}">{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary" >{{__("labels.submit")}}</button><a href="{{url('supplier')}}"> <button type="button" class="btn btn-secondary" >{{__("labels.cancel")}}</button></a>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /Validation -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection

@section('page-script')
    <script src="{{ asset('js/supplier.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection