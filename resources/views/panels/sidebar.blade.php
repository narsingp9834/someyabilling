@php
$configData = Helper::applClasses();
@endphp
<div class="main-menu menu-fixed {{($configData['theme'] === 'dark') ? 'menu-dark' : 'menu-light'}} menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav" style="align-items: center;">
      <li class="nav-item">
        <a class="navbar-brand" href="{{url('/home')}}">
          <!-- <span class="brand-logo"> -->
            <img src="{{asset('web_assets/images/someyalogo.jpeg')}}" style="width: 100%;
            margin-top: -5px;width: 160px;
    height: 67px;"/>
          <!-- </span> -->
          <!-- <h2 class="brand-text">Vuexy</h2> -->
        </a>
      </li>
      <!-- <li class="nav-item nav-toggle">
        <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
          <i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i>
          <i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i>
        </a>
      </li> -->
    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        @php $allMenuData = array(); @endphp
        @if(Auth::user()->user_type == 'SA')
            @php $allMenuData = $menuData[0]; @endphp
        @elseif(Auth::user()->user_type == 'B')
            @php $allMenuData = $menuData[2]; @endphp
        @elseif(Auth::user()->user_type == 'S')
          @php $allMenuData = $menuData[3]; @endphp
        @elseif(Auth::user()->user_type == 'Buyer')
        @php $allMenuData = $menuData[4]; @endphp  
        @endif

        {{-- Foreach menu item starts --}}
        @if(isset($allMenuData) && !empty($allMenuData))
          @foreach($allMenuData->menu as $menu)
            @if(isset($menu->navheader))
              <li class="navigation-header">
                <span>{{ __('locale.'.$menu->navheader) }}</span>
                <i data-feather="more-horizontal"></i>
              </li>
            @else
              {{-- Add Custom Class with nav-item --}}
              @php
              $custom_classes = "";
              if(isset($menu->classlist)) {
              $custom_classes = $menu->classlist;
              }
            @endphp
              <li class="nav-item @if(Request::is($menu->slug.'/*') || Request::is($menu->slug)) active  @endif" {{ $custom_classes }}">
                <a href="{{isset($menu->url)? url($menu->url):'javascript:void(0)'}}" class="d-flex align-items-center" target="{{isset($menu->newTab) ? '_blank':'_self'}}">
                  <i data-feather="{{ $menu->icon }}"></i>
                  <span class="menu-title text-truncate">{{ __('locale.'.$menu->name) }} @if ($menu->name == 'Purchase')
                    <span class="badge badge-success" style="background-color: #28a745">Care</span>
                  @endif
                  @if ($menu->name == 'GST Reports')
                    <span class="badge badge-success">Alert</span>
                  @endif
                @if ($menu->name == 'Loan Offer')
                    <span class="badge badge-success" >Best Deal</span>
                  @endif
                </span>
                  @if (isset($menu->badge))
                  <?php $badgeClasses = "badge badge-pill badge-light-primary ml-auto mr-1" ?>
                  <span class="{{ isset($menu->badgeClass) ? $menu->badgeClass : $badgeClasses }} ">{{$menu->badge}}</span>
                  @endif
                </a>
                @if(isset($menu->submenu))
                @include('panels/submenu', ['menu' => $menu->submenu])
                @endif
              </li>
            @endif
          @endforeach
        @endif
        {{-- Foreach menu item ends --}}

        @if(Auth::user()->user_type == 'A')
          <li class="nav-item @if(Request::is('home/*') || Request::is('home')) active  @endif">
              <a href="{{url('home')}}" class="d-flex align-items-center">
                <i data-feather="home"></i>
                <span class="menu-title text-truncate">Dashboard</span>
              </a>
            </li>
          @foreach($menus as $menu)
              <li class="nav-item @if(Request::is($menu->slug.'/*') || Request::is($menu->slug)) active  @endif">
                <a href="{{isset($menu->menu_url)? url($menu->menu_url):'javascript:void(0)'}}" class="d-flex align-items-center">
                  <i data-feather="{{ $menu->icon }}"></i>
                  <span class="menu-title text-truncate">{{ $menu->menu_show_name }}</span>
                </a>
                @if(isset($menu->getSubmenu) && ($menu->getSubmenu)->isNotEmpty())
                  <ul class="menu-content">
                  @foreach($menu->getSubmenu as $submenu)
                    @if(in_array($submenu->id,$subMenus))
                    <li class="@if(Request::is($submenu->submenu_slug.'/*') || Request::is($submenu->submenu_slug)) active  @endif" ">
                      <a href="{{isset($submenu->submenu_url) ? url($submenu->submenu_url):'javascript:void(0)'}}" class="d-flex align-items-center" >
                        <i data-feather="{{$submenu->submenu_icon}}"></i>
                        <span class="menu-item text-truncate">{{ $submenu->submenu_name  }}</span>
                      </a>
                    </li>
                    @endif
                    @endforeach
                    </ul>
                @endif
              </li>
          @endforeach
        @endif
    </ul>
  </div>
</div>
<!-- END: Main Menu-->
