{{-- Vendor Scripts --}}
<script src="{{ asset(mix('vendors/js/vendors.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/ui/prism.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@yield('vendor-script')

{{-- Theme Scripts --}}
{{-- @toastr_js
@toastr_render --}}
<script src="{{ asset(mix('js/core/app-menu.js')) }}"></script>
<script src="{{ asset(mix('js/core/app.js')) }}"></script>
@if($configData['blankPage'] === false)
<script src="{{ asset(mix('js/scripts/customizer.js')) }}"></script>
@endif

<script type="text/javascript">

     $(document).ready(function () {
        var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        $("#timeZone").val(timeZone);
    });
</script>
{{-- page script --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="{{ asset('js/custom.js') }}?v={{Config::get('constants.portal_version')}}"></script>
<script src="{{ asset('js/scripts/toastr.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript">
    $(document).on('click', '.logout', function (e ) {
       var message = "Are you sure you want to logout?";
       var id = $(this).data('id');
       bootbox.confirm({
           title: "Logout",
           message: message,
           buttons: {
               confirm: {
                   label: 'Yes',
                   className: 'btn-primary'
               },
               cancel: {
                   label: 'No',
                   className: 'btn-secondary'
               }
           },
           callback: function (result) {
               if(result == true){
                   var urls="{{url('logout')}}";
                   window.location.href = urls;
               }
           }
       });
    });
</script>

{{-- @toastr_js
@toastr_render --}}
@yield('page-script')
{{-- page script --}}
<script src="{{ asset('js/validation.js') }}"></script>
<script>
    $('.select2').select2({
        width: '100%',
    });

    $('.flatpickr').flatpickr({
        dateFormat: "Y-m-d"
    });
    $(document).ready(function () {

        $(document).on('select2:open', (e) => {
            $('.select2-search__field').attr('tabindex', 0);
            setTimeout(function() {
                document.querySelector(".select2-container--open .select2-search__field").focus();
            }, 10);            
        });
    });
</script>