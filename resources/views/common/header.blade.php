
    <!---======================Header Start Here =====================================-->
    <nav class="navbar navbar-expand-lg fixed-top navbar-custom">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('assets/images/logo.jpeg')}}" class="img-fluid"
                    width="140px"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar"
                aria-controls="offcanvasNavbar" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar"
                aria-labelledby="offcanvasNavbarLabel" style="width:330px;background-color:#333">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasNavbarLabel"><a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('assets/images/logo.jpeg')}}" class="img-fluid"
                    width="140px"></a></h5>
                    
                    <i class="bi bi-x-lg close-btnn" data-bs-dismiss="offcanvas" aria-label="Close"></i>

                
                </div>
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-start flex-grow-1 pe-3">
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/')}}">Home</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{url('contact-us')}}">Careers</a>
                        </li>
                        
                         <li class="nav-item">
                            <a class="nav-link" href="{{url('category/products/4')}}">Tax Blog</a>
                        </li>

                        
                         <li class="nav-item">
                            <a class="nav-link" href="{{url('category/products/1')}}">Invoice Software</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{url('category/products/2')}}">Best Web Solutions</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="{{url('category/products/3')}}">Web Learning</a>
                        </li>
                    </ul>

                    <form class="d-flex">

                        <a href="{{url('seller/login')}}" class="btn custom-btn ">Free Demo for 7 days</a>
                        {{-- <a href="signup.html" class="btn custom-btn signup">Sign Up</a> --}}

                    </form>
                </div>
            </div>
        </div>
    </nav>
    <!---======================Header End Here =====================================-->