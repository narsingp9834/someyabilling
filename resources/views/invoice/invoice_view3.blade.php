<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>

<style>
    body {
        padding: 0;
        margin: 0;
        font-family: "Poppins", sans-serif;
    }

    .poppins-light {
        font-family: "Poppins", sans-serif;
        font-weight: 300;
        font-style: normal;
    }

    .poppins-regular {
        font-family: "Poppins", sans-serif;
        font-weight: 400;
        font-style: normal;
    }

    .poppins-medium {
        font-family: "Poppins", sans-serif;
        font-weight: 500;
        font-style: normal;
    }

    .poppins-semibold {
        font-family: "Poppins", sans-serif;
        font-weight: 600;
        font-style: normal;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Poppins", sans-serif;
        font-weight: 500;
        font-style: normal;
    }

    p {
        font-family: "Poppins", sans-serif;
        font-weight: 400;
        font-style: normal;
    }

    .invoice-card {
        width: 760px;
        margin-left:-2rem;
        height:100vh;
        
        border-radius: 5px;
        margin-top: 2rem;
        border: 1px solid gray;
        margin-left: -7rem;
        margin-right: -7rem;
        
    }

    table {

        border-collapse: collapse;
        width: 100%;
    }

    .invoice-heading {
        margin-bottom: 10px;

    }

    .invoice-card-right {

        padding: 10px 20px;
        text-align: end;
    }

    .invoice-card-left {

        padding: 10px 20px;
    }

    .invoice-card-left h6 {

        font-size: 14px;
        margin-bottom: 2px;
    }

    .invoice-card-left p {

        font-size: 14px;
        margin-bottom: 5px;
    }

    .invoice-card-sc {
        padding: 25px 0px;
    }

    .table-tag {
        border-collapse: collapse;
        width: 100%;
    }

    .table-tag td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 4px;
    }

    .table-tag-sc {
        font-size: 14px;
    }

    .dis {
        padding: 10px 10px;


    }

    .dis p {
        font-size: 14px;
        margin-bottom: 5px;

    }

    .dis h6 {
        font-size: 14px;
    }

    .text-right {
        text-align: right !important;
    }
    .img-sc{
        text-align:center;
    }
</style>
</head>

<body>


    <!--================= Invoice Start Here ===================-->
    <div class="container">
        @if ($data['invoice']->invoice_type == 'Invoice')
            <p style="text-align:center;font-size:25px;">Tax Invoice </p>         
        @elseif($data['invoice']->invoice_type == 'Quotation')
            <p style="text-align:center;font-size:25px;">Quotation</p> 
        @else
            <p style="text-align:center;font-size:25px;">Delivery Chalan</p>    
        @endif
        <div class="invoice-card">
            
            <table>
                <tr class="invoice-card-sc">
                    <th class="invoice-card-left">
                        <h6>E-Khatabook.com</h6>
                        <p>Sold by: {{$data['user']->user_fname}}</p>
                        <h6>GST No:{{$data['user']->getSeller->gst_no}}</h6>
                        <h6>Phone No:{{$data['user']->user_phone_no}}</h6>
                        <h6>Email Id: {{$data['user']->email}}</h6>
                        <h6>Address :{{$data['user']->getSeller->address}} {{$data['user']->getSeller->getCity->name}} , {{$data['user']->getSeller->getState->name}}
                        </h6>
                    </th>
                    <th class="invoice-card-right">
                        <h5  style="text-align:center">Invoice</h5>
                        <div class="img-sc">
                             <img alt="testing" src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl={{$data['user']->getSeller->scanner_link}}&choe=UTF-8" />
                        </div>
                       
                    </th>

                </tr>

                <tr class="invoice-card-sc">
                    <th class="invoice-card-left">
                        <h5>Shipping Address</h5>
                        <p>Firm Name :{{$data['buyer']->getUser->user_fname}}</p>
                        <p>GST No/Pan No : {{$data['buyer']->gst_no}}</p>
                        <h6>Phone No :{{$data['buyer']->getUser->user_phone_no}}</h6>
                        <h6>Pin No :{{$data['buyer']->pin_no}}</h6>
                        <p>City : {{$data['buyer']->getCity->name}}</p>
                        </p>State : {{$data['buyer']->getState->name}}</p>
                        <p>Address :{{$data['buyer']->address}}
                        </p>





                    </th>
                    <th class="invoice-card-left">
                        <h5>Billing Details</h5>
                        <p>Invoice No:{{$data['invoice']->invoice_no}}</p>
                        <p>Invoice Date: {{Helper::getDate($data['invoice']->created_at)}}
                        </p>
                        <p>Order Date: {{Helper::getDate($data['invoice']->created_at)}}</p>
                        <p>Payment Mode: {{$data['invoice']->payment_mode}}</p>


                    </th>

                </tr>


            </table>

            <table class="table-tag">
                <tr>
                    <th>Sr No</th>
                    <th>Item Name</th>
                    <th>HSN</th>
                    <th>QTY</th>
                    <th>Rate</th>
                    <th>Per</th>
                    <th>Disc(%)</th>
                    <th>TOTAL</th>
                </tr>
                @php
                    $totalProdPrice = 0;
                @endphp
                @foreach ($data['invoice']->products as $key => $product )
                    {{-- @php
                            $uProduct = Product::find($product->product_id);
                            // dd($uProduct);
                    @endphp --}}
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$product->getProduct->product_name}}</td>
                        <td>{{$data['invoice']->getHSN->code}}</td>
                        <td>{{$product->quantity}}</td>
                        <td>{{$product->price}}</td> 
                        <td>{{$product->getProduct->unit->name}}</td>
                        <td>{{$data['invoice']->discount}}</td>
                        <td>{{$product->quantity * $product->price}}</td>
                    </tr>
                    @php
                        $totalProdPrice += $product->quantity * $product->price;
                    @endphp
                @endforeach


            </table>
            <table class="table-tag table-tag-sc">

                <tr>
                    <td style="width: 85%;text-align: right;">Value</td>
                    <td>₹{{$totalProdPrice - ($totalProdPrice * $data['invoice']->discount/100)}}</td>

                </tr>

                @php
                        $totalProdPrice = $totalProdPrice- ($totalProdPrice * ($data['invoice']->discount/100));
                @endphp

                <tr>
                    <td style="width: 85%;text-align: right;">IGST ({{$data['invoice']->igst}}%)</td>
                    <td>₹{{Helper::getTaxAmount($totalProdPrice,$data['invoice']->igst) }}</td>

                </tr>
                <tr>
                    <td style="width: 85%;text-align: right;">CGST ({{$data['invoice']->cgst}}%)</td>
                    <td>₹{{Helper::getTaxAmount($totalProdPrice,$data['invoice']->cgst) }}</td>

                </tr>
                <tr>
                    <td style="width: 85%;text-align: right;">SGST ({{$data['invoice']->sgst}}%)</td>
                    <td>₹{{Helper::getTaxAmount($totalProdPrice,$data['invoice']->sgst) }}</td>

                </tr>
                <tr>
                    <td style="width: 85%;text-align: right;">Grand Total(inclusive of all taxes)</td>
                    <td>₹{{$totalProdPrice + Helper::getTaxAmount($totalProdPrice,$data['invoice']->cgst + $data['invoice']->sgst + $data['invoice']->igst)}}</td>

                </tr>
                <tr>
                    <td style="width: 85%;text-align: right;">Advance</td>
                    <td>₹{{$data['invoice']->advance_amount}}</td>

                </tr>
                <tr>
                    <td style="width: 85%;text-align: right;">Total(Remaining Balance)</td>
                    <td>₹{{$totalProdPrice + Helper::getTaxAmount($totalProdPrice,$data['invoice']->cgst + $data['invoice']->sgst + $data['invoice']->igst) -$data['invoice']->advance_amount}}</td>

                </tr>




            </table>
            <div class="dis">
                <h6>Build Business With Us:
                </h6>
                <p>{{$data['user']->getSeller->description}}
                </p>
               
                <h6>{{$data['user']->getSeller->address}}</h6>
            </div>

        </div>
    </div>

    <!--================= Invoice End Here ===================-->
  
    <div class="container">
        <div class="invoice-card">
            <table>
                <tr class="invoice-card-sc">
                    <th class="invoice-card-left">
                        <h6>E-Khatabook.com</h6>
                        <p>Sold by: {{$data['user']->user_fname}}</p>
                        <h6>GST No:{{$data['user']->getSeller->gst_no}}</h6>
                        <h6>Phone No:{{$data['user']->user_phone_no}}</h6>
                        <h6>Email Id: {{$data['user']->email}}</h6>
                        <h6>Address :{{$data['user']->getSeller->address}} {{$data['user']->getSeller->getCity->name}} , {{$data['user']->getSeller->getState->name}}
                        </h6>
                    </th>
                     <th class="invoice-card-right">
                        <h5  style="text-align:center">Invoice</h5>
                        <div class="img-sc">
                             <img alt="testing" src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=test&choe=UTF-8" />
                        </div>
                       
                    </th>

                </tr>

                <tr class="invoice-card-sc">
                    <th class="invoice-card-left">
                        <h5>Shipping Address</h5>
                        <p>Firm Name :{{$data['buyer']->getUser->user_fname}}</p>
                        <p>GST No/Pan No : {{$data['buyer']->gst_no}}</p>
                        <h6>Phone No :{{$data['buyer']->getUser->user_phone_no}}</h6>
                        <h6>Pin No :{{$data['buyer']->pin_no}}</h6>
                        <p>City : {{$data['buyer']->getCity->name}}</p>
                        </p>State : {{$data['buyer']->getState->name}}</p>
                        <p>Address :{{$data['buyer']->address}}
                        </p>





                    </th>
                    <th class="invoice-card-left">
                        <h5>Billing Details</h5>
                        <p>Invoice No:{{$data['invoice']->invoice_no}}</p>
                        <p>Invoice Date: {{Helper::getDate($data['invoice']->created_at)}}
                        </p>
                        <p>Order Date: {{Helper::getDate($data['invoice']->created_at)}}</p>
                        <p>Payment Mode: {{$data['invoice']->payment_mode}}</p>


                    </th>

                </tr>


            </table>



        </div>
    </div>

</body>

</html>