
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Invoice</title>
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
		<link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
		{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}
		<style>
			body {
            overflow-x: hidden;
            font-family: 'Roboto Slab', -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji';
			background: lavender ;
			color:black;
			margin:auto;
			height:297mm;
			width:220mm;
			}
			p,a{
			color:black;
			font-size:16px;
			}
			.signature {
            font-family: 'Kaushan Script', -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji';
            color: #0099D5;
			}
			
			.table thead th {
            vertical-align: bottom;
            padding: 6px;
            font-size: 17.5px;
			}
			
			.table tbody td {
            padding: 2px 8px;
			}
			.table-bordered>:not(caption)>* {
			border-width: 1px 1px;
			}
			
			.font-weight-lighter {
            font-weight: bolder;
			}
			
			.inv {
			
			padding-top:3rem;
			}
			.table, .table-hover tbody tr:hover {
			color: #0c0c0c !important;
			}
			
			.my-3 {
            margin-bottom: 1rem !important;
            margin-top: 1rem !important;
			}
			
			.my-5 {
            margin-bottom: 3rem !important;
            margin-top: 3rem !important;
			}
			
			.py-5 {
            padding-bottom: 3rem !important;
            padding-top: 3rem !important;
			}
			
			.px-3 {
            padding-left: 1rem !important;
            padding-right: 1rem !important;
			}
			
			.border-bottom {
            border-bottom: 2px solid #000 !important;
            padding: 0px 0px 5px 0px;
			}
			
			@media print {
            .printInvoice{
			display: none;
            }
			html body{
			background:white;
			}
			}
			#logo_img{
			text-align: center;
			}
			/* #invoiceid{
			<!-- flex-wrap: nowrap; -->
			} */
			.border_sp{
			padding-bottom:8px;
			border-bottom:1px solid gray;
			}
			.font-weight-bold {font-weight:bold;}
			
			.table td, .table th {
			padding: 0.2rem;
			vertical-align: top;
			border-top: 0 !important;
			}
			b{
			font-weight:500;
			}
		</style>
	</head>
	<body>
			<div class="container-fluid inv p-4" id="pdf" style="background:white; color:black !important; ">
				<div class="row" style="border: 2px solid grey;" > 
					<div class="col-6 py-1 px-3" style="border-right: 2px solid grey;"> 
						<p class="mb-2"><a href="base_url()"><b>Taaycoon.com</b></a></p>
						<div class="row">
							<div class="col-8">
								<p class="mb-0">Sold by: {{$data['user']->user_fname}}
							<br>
							
								{{$data['user']->getSeller->address}} {{$data['user']->getSeller->getCity->name}} {{$data['user']->getSeller->getState->name}},</p>
								<p class="mb-0"><b>GST No:{{$data['user']->getSeller->gst_no}}</b></p>
								<p class="mb-0"><b>Phone No:{{$data['user']->user_phone_no}}</b></p>
							</div>
						</div>
					</div>
					<div class="col-6 d-flex flex-column align-items-end">
						<h1 class="font-weight-lighter pb-0">
                            @if ($data['invoice']->invoice_type == 'Invoice')
                                INVOICE
                            @elseif($data['invoice']->invoice_type == 'Quotation')
                                Quotation
                            @else
                                Delivery Chalan    
                            @endif
                        </h1>
						 <div class="mb-1">
							<img alt="testing" src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=test&choe=UTF-8" />
						</div> 
					</div>
				</div>
				
				<div class="row mb-0" style="border: 2px solid grey;border-top: unset;">
					<div class="col-7" style="border-right:2px solid grey;">
						<p class="font-weight-bold mb-0">Shipping Address</p>
						
					
					    <p class="mb-0"><b>Firm Name:</b>{{$data['buyer']->name}}</p>
					    <p class="mb-0"><b>GST No/Pan No:</b>{{$data['buyer']->gst_no}}</p>
						<p class="mb-0"><b>Mobile No:</b>{{$data['buyer']->phone_no}}</p>
						<p class="mb-0"><b>Pin No:</b>{{$data['buyer']->pin_no}}</p>
						<p class="mb-0"><b>City:</b>{{$data['buyer']->getCity->name}}<br>State:{{$data['buyer']->getState->name}}</p>
						<p class="mb-0">{{$data['buyer']->address}}</p>
					</div>
					<div class="col-5">
						<p class="mb-0 mt-2"><b><span>Invoice No:</span></b>{{$data['invoice']->invoice_no}}</p>
						<p class="mb-0"><b><span>Invoice Date: </span></b>{{Helper::getDate($data['invoice']->created_at)}}</p>
						<p class="mb-0"><b><span>Payment Mode: </span></b>{{$data['invoice']->payment_mode}}</p>
					</div>
				</div>
			
				<div class="row mb-2" >
					<div class="col-sm-12 p-0">
						<div class="row m-0" style="border: 2px solid grey;">
							<div class="col-lg-12 p-0">
								<table class="table table-bordered text-center table-responsive">
									<thead>
										<tr>
											<th scope="col">Item Name</th>
											<th scope="col">HSN</th>
											<!-- <th scope="col">SIZE</th> -->
											<th scope="col">QTY</th>
											<th scope="col">UNIT PRICE</th>
											<th scope="col">TAX RATE</th>
                                            <th scope="col">TAX Amount</th>
											<th scope="col">TOTAL</th>
										</tr>  
									</thead>
									<tbody>
											@php
												$totalProdPrice = 0;	
											@endphp
                                                @foreach ($data['invoice']->products as $product)
                                                <tr>  
                                                    <td style="text-align:start;">
														<p class="mb-0">{{$product->product_name}}</p>
													</td>
													<td>{{$data['invoice']->getHSN->code}}</td>
													<td>{{$product->quantity}}</td>
                            
                                                    <td>{{$product->price}}</td>
                                                
                                                    <td>GST ({{$data['invoice']->cgst + $data['invoice']->sgst + $data['invoice']->igst}}%)</td>
                                                    <td>{{Helper::getTaxAmount($product->quantity * $product->price,$data['invoice']->cgst + $data['invoice']->sgst + $data['invoice']->igst) }}</td>
                                                    <td>{{$product->quantity * $product->price}}</td>

													@php
															$totalProdPrice += $product->quantity * $product->price;
													@endphp
                                                </tr>
                                                @endforeach
									
					

					
                    @if ($data['invoice']->igst != 0) 
						<tr><td colspan="5" style="text-align:end;"><b>IGST Amount ({{$data['invoice']->igst}}%)</b></td>
						<td><b>&nbsp; {{Helper::getTaxAmount($totalProdPrice,$data['invoice']->igst)}}</b></td></tr>
                    @endif    
					
                    @if ($data['invoice']->cgst != 0)     
						<tr><td colspan="5" style="text-align:end;"><b>CGST Amount ({{$data['invoice']->cgst}}%)</b></td>
						<td><b>&nbsp;{{Helper::getTaxAmount($totalProdPrice,$data['invoice']->cgst)}}</b></td></tr>
                    @endif      
					
                    @if ($data['invoice']->sgst != 0)         
						<tr><td colspan="5" style="text-align:end;"><b>SGST Amount ({{$data['invoice']->sgst}}%)</b></td>
						<td><b>&nbsp;{{Helper::getTaxAmount($totalProdPrice,$data['invoice']->sgst)}}</b></td></tr>
                    @endif  
					
									<tr>
										<td colspan="5" style="text-align:end;"><b>Grand Total(inclusive of all taxes)</b></td>
										@php
											$totalWithTax = $totalProdPrice + Helper::getTaxAmount($totalProdPrice,$data['invoice']->igst + $data['invoice']->cgst + $data['invoice']->sgst)	
										@endphp
										<td><b>{{$totalWithTax}}</b></td>
									</tr>
									



									<tr>
										<td colspan="5" style="text-align:end;"><b>Discount({{$data['invoice']->discount}}%)</b></td>
										<td><b>
											&nbsp;
											{{Helper::getTaxAmount($totalWithTax,$data['invoice']->discount)}}
										</b></td>
									</tr>
										<tr>
										<td colspan="5" style="text-align:end;"><b>Advance</b></td>
										<td><b>
											&nbsp;
											{{$data['invoice']->advance_amount}}
										</b></td>
									</tr>
									<tr>
										<b><td colspan="5" style="text-align:end; color:black;">Total(Remaining Balance)</td></b>
										<td><b>
											&nbsp;
											{{$data['invoice']->grand_total}}
										</b></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row m-0" style="border:2px solid grey; border-top:unset;">
						<div class="col-sm-12"> 
							<b>Build Business With Us:</b><br>
							<p class="mb-0">{{$data['user']->getSeller->description}}</p>
						</div>
					</div>
					<br><br>	<br><br> <br><br><br>	<br><br><br><br><br><br><br><br>									
						<p class="my-2"><b style="display: inline-block; width: 44%; border-bottom: 2px dashed black;"></b><b style="position: relative;top: 4px;margin: 0px 6px;">Fold Here</b><b style="display: inline-block; width: 44%; border-bottom: 2px dashed black;"></b></p>
					<div class="row" style="border: 2px solid grey;" > 
                        <div class="col-6 py-1 px-3" style="border-right: 2px solid grey;"> 
                            <p class="mb-2"><a href="base_url()"><b>Taaycoon.com</b></a></p>
                            <div class="row">
                                <div class="col-8">
                                    <p class="mb-0">Sold by: {{$data['user']->user_fname}}
                                <br>
                                
                                    {{$data['user']->getSeller->address}} {{$data['user']->getSeller->getCity->name}}{{$data['user']->getSeller->getState->name}},</p>
                                    <p class="mb-0"><b>GST No:{{$data['user']->getSeller->gst_no}}</b></p>
                                    <p class="mb-0"><b>Phone No:{{$data['user']->user_phone_no}}</b></p>
                                </div>
                            </div>
                        </div>

						<div class="col-6 d-flex flex-column align-items-end">
							<h1 class="font-weight-lighter pb-0">
								@if ($data['invoice']->invoice_type == 'Invoice')
									INVOICE
								@elseif($data['invoice']->invoice_type == 'Quotation')
									Quotation
								@else
									Delivery Chalan    
								@endif
							</h1>
							 <div class="mb-1">
								<img alt="testing" src="https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl={{$data['user']->getSeller->scanner_link}}&choe=UTF-8" />
							</div> 
						</div>
				    </div>
			
					<div class="row mb-0" style="border: 2px solid grey;border-top: unset;">
						<div class="col-7" style="border-right:2px solid grey;">
							<p class="font-weight-bold mb-0">Shipping Address</p>
							
						
							<p class="mb-0"><b>Firm Name:</b>{{$data['buyer']->name}}</p>
							<p class="mb-0"><b>GST No/Pan No:</b>{{$data['buyer']->gst_no}}</p>
							<p class="mb-0"><b>Mobile No:</b>{{$data['buyer']->phone_no}}</p>
							<p class="mb-0"><b>Pin No:</b>{{$data['buyer']->pin_no}}</p>
							<p class="mb-0"><b>City:</b>{{$data['buyer']->getCity->name}}<br>State:{{$data['buyer']->getState->name}}</p>
							<p class="mb-0">{{$data['buyer']->address}}</p>
						</div>
						<div class="col-5">
							<p class="mb-0 mt-2"><b><span>Invoice No:</span></b>{{$data['invoice']->invoice_no}}</p>
							<p class="mb-0"><b><span>Invoice Date: </span></b>{{Helper::getDate($data['invoice']->created_at)}}</p>
							<p class="mb-0"><b><span>Payment Mode: </span></b>{{$data['invoice']->payment_mode}}</p>
						</div>
					</div>
				</div>
				</body>
			
		</html>