@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
           
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <form id="jquery-val-form" method="POST" action="{{url('hsn/update')}}" autocomplete="off">
                    @csrf
                    <input type="hidden" name="hsn_id" value="{{$hsn->hsn_id}}">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">{{__("labels.edit")}} HSN</h5>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="">Code</label>
                                        <input type="text" name="code" id="code" class="form-control" value="{{$hsn->code}}"> 
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="">Percentage</label>
                                        <input type="text" name="percentage" id="percentage" class="form-control" value="{{$hsn->percentage}}"> 
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary" >{{__("labels.submit")}}</button><a href="{{url('hsn')}}"> <button type="button" class="btn btn-secondary" >{{__("labels.cancel")}}</button></a>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /Validation -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection

@section('page-script')
    <script src="{{ asset('js/hsn.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection