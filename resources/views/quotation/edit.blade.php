@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
           
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="jquery-val-form" method="POST" action="{{url('quotation/update')}}" autocomplete="off" class="add-invoice">
                    @csrf
                    <input type="hidden" name="invoice_id" value="{{$invoice->invoice_id}}">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">{{__("labels.edit")}} Quotation</h5>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="">Buyer Name</label>
                                        <select class="form-control" name="buyer_id" id="buyer_id" required>
                                                <option value="">Select Buyer</option>
                                                @foreach ($buyers as $buyer)
                                                    <option value="{{$buyer->buyer_id}}" {{($buyer->buyer_id == $invoice->buyer_id) ? 'selected' : ''}}>{{$buyer->user_fname}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">Invoice No</label>
                                        <input type="text" class="form-control" name="invoice_no" id="invoice_no"
                                            value="{{$invoice->invoice_no}}">
                                    </div>
                                </div>


                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">HSN</label>
                                        <select class="form-control" name="hsn_id" id="hsn_id" required>
                                                <option value="">Select HSN</option>
                                                @foreach ($hsns as $hsn)
                                                        <option value="{{$hsn->hsn_id}}" {{($hsn->hsn_id == $invoice->hsn_id) ? 'selected' : ''}}>{{$hsn->code}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>



                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">CGST</label>
                                        <input type="text" class="form-control" name="cgst" id="cgst"
                                            readonly value="{{$invoice->cgst}}">
                                    </div>

                                </div>

                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">SGST</label>
                                        <input type="text" class="form-control" name="sgst"
                                        id="sgst"  readonly value="{{$invoice->sgst}}">
                                    </div>

                                </div>

                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">IGST</label>
                                        <input type="text" class="form-control" name="igst" id="igst"
                                             readonly value="{{$invoice->igst}}">
                                    </div>

                                </div>

                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">Date</label>
                                        <input type="text" class="form-control bg-white" name="date" id="date" value="{{$invoice->date}}" required>
                                    </div>

                                </div>
                            </div>   
                                <div class="justify-content-end align-items-end">
                                    <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                        <i data-feather="plus" class="mr-25"></i>
                                        <span>Add New</span>
                                    </button>

                                    <button class="btn btn-icon btn-primary calculate" type="button">
                                        <span>Calculate Total</span>
                                    </button>
                                </div>
                                          
                            <input type="hidden" id="totalRows" value="1">
                            <input type="hidden" id="productCount" value="{{count($invoice->products)}}">
                                <div data-repeater-list="columns">
                                    @foreach($invoice->products as $key => $iproduct)  
                                        <div class="div-repeat" data-repeater-item>
                                            <input type="hidden" name="invoice_product_id" value="{{$iproduct->invoice_product_id}}">
                                            <div class="row">
                                                <div class="col-md-4 col-6">
                                                    <div class="form-group">
                                                        <label id="for" class="">Product</label>
                                                        <select class="form-control" name="product_id" id="product_id" required>
                                                                <option value="">Select Product</option>
                                                                @foreach ($products as $product)
                                                                    @if ($product->available_stock > 0)
                                                                        <option value="{{$product->product_id}}" {{($product->product_id == $iproduct->product_id) ? 'selected' : ""}}>{{$product->product_name}}
                                                                            ({{$product->available_stock}})</option>
                                                                    @endif        
                                                                @endforeach
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-md-3 col-6">
                                                    <div class="form-group">
                                                        <label id="for" class="">Quantity</label>
                                                        <input type="text" class="form-control quantity" name="quantity" id="quantity{{$key}}"
                                                            placeholder="Enter Quantity" required value="{{$iproduct->quantity}}">
                                                    </div>
                                                </div>


                                                <div class="col-md-3 col-6">
                                                    <div class="form-group">
                                                        <label id="for" class="">Price</label>
                                                        <input type="text" class="form-control price" name="price" id="price{{$key}}"
                                                            placeholder="Enter Price" required value="{{$iproduct->price}}">
                                                    </div>
                                                </div>

                                                <div class="col-md-2 col-12">
                                                    <div class="form-group">
                                                        <label class="form-label" for="customer_type"></label>
                                                        <button class="btn btn-outline-danger text-nowrap px-1 form-control deleteProduct" data-repeater-delete type="button"  data-page="edit" data-id="{{$iproduct->invoice_product_id}}">
                                                            <i data-feather="x" class="mr-25 text-danger"></i>
                                                            <span class="text-danger">Delete</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    @endforeach    
                                </div>
                            <div class="row">

                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">GST Amount</label>
                                        <input type="text" class="form-control" name="gst_amount" id="gst_amount"
                                            readonly value="{{$invoice->gst_amount}}">
                                    </div>
                                </div>

                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">Total</label>
                                        <input type="text" class="form-control" name="total" id="total" value="{{$invoice->total}}" readonly>
                                    </div>
                                </div>


                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">Discount</label>
                                        <input type="text" class="form-control" name="discount" id="discount" value="{{$invoice->discount}}">
                                    </div>
                                </div>

                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">Advance</label>
                                        <input type="text" class="form-control" name="advance_amount" id="advance_amount" value="{{$invoice->advance_amount}}">
                                    </div>
                                </div>

                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">Grand Total</label>
                                        <input type="text" class="form-control" name="grand_total" id="grand_total" value="{{$invoice->grand_total}}" readonly>
                                    </div>
                                </div>

                                <div class="col-md-4 col-6">
                                    <div class="form-group">
                                        <label id="for" class="">Payment Mode</label>
                                        <select class="form-control" name="payment_mode" id="payment_mode" required>
                                                <option value="">Select</option>
                                                <option value="Dacc" {{($invoice->payment_mode == 'Dacc') ? 'selected' : ''}}>Dacc</option>
                                                <option value="Paid" {{($invoice->payment_mode == 'Paid') ? 'selected' : ''}}>Paid</option>
                                                <option value="Credit" {{($invoice->payment_mode == 'Credit') ? 'selected' : ''}}>Credit</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary" >{{__("labels.submit")}}</button><a href="{{url('invoice')}}"> <button type="button" class="btn btn-secondary" >{{__("labels.cancel")}}</button></a>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /Validation -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/repeater/jquery.repeater.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection

@section('page-script')
    <script src="{{ asset('js/invoice.js') }}?v={{Config::get('constants.portal_version')}}"></script>
    <script src="{{ asset(mix('js/scripts/forms/form-repeater.js')) }}"></script>
@endsection