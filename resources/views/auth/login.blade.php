@extends('layouts/fullLayoutMaster')

@section('title', 'Login')

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/base/pages/page-auth.css')) }}">
<link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
<div class="auth-wrapper auth-v1 px-2">
  <div class="auth-inner py-2">
    <!-- Login v1 -->
    <div class="card mb-0">
      <div class="card-body">
        <a href="javascript:void(0);" class="brand-logo">
            <img src="{{asset('web_assets/images/someyalogo.jpeg')}}" style="
            width: 300px;
            height: 200px;
        "/>
        </a>
        <h4 class="card-title mb-1">Welcome to SOMEYA READYMADE ! 👋</h4>
        <p class="mb-2">Please sign-in to your account</p>

        <form class="auth-login-form mt-2" method="POST" action="{{ route('login') }}" id="jquery-val-form" autocomplete="off">
          @csrf
          <div class="form-group">
            <label for="login-email" class="form-label">Phone no</label>
            <input type="text" class="form-control @error('email') is-invalid @enderror" id="login-email" name="email" placeholder="Phone no" aria-describedby="login-email" tabindex="1" autofocus value="{{ old('email') }}" required="" maxlength="50" />
            @error('email')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
            @enderror
          </div>

          <div class="form-group">
            <div class="d-flex justify-content-between">
              <label for="login-password">Password</label>
            </div>
            <div class="input-group input-group-merge form-password-toggle">
              <input type="password" class="form-control form-control-merge" id="password" name="password" tabindex="2" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" required="" minlength="8" maxlength="30" />
              <div class="input-group-append">
                <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
              </div>
            </div>
          </div>
          
          <div class="custom-control custom-checkbox mb-1">
            <input type="checkbox" class="custom-control-input" checked disabled/>
            <label class="custom-control-label" for="sameAsAbove" value="on" style="font-size:1rem; color:#5e5873">&nbsp;By proceeding, you agree to our Terms of Use  and Privacy Policy</label>
        </div>
          {{-- <input type="checkbox" class="custom-control-input"/><p class="mb-2">By proceeding, you agree to our Terms of Use  and Privacy Policy</p> --}}

          <button type="submit" class="btn btn-primary btn-block" tabindex="4" id="loginBtn">Sign in</button>
        </form>
      </div>
    </div>
    <!-- /Login v1 -->
  </div>
</div>
@endsection
@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
<script>
    $(function () {
        var jqForm = $('#jquery-val-form');
        if (jqForm.length) {
            jQuery.validator.addMethod("validate_email", function(value, element) {
                if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
                    return true;
                } else {
                    return false;
                }
            }, "Please enter valid email");
            jqForm.validate({
              rules: {
                    email: {
                        required: true,
                    },
                    password: {
                        required: true,
                        nospaces: true,
                        minlength: 8,
                        maxLength: 30,
                    },
                },
                messages: {
                    email: {
                        required: "Please enter email address",
                        email: "Please enter valid email",
                        validate_email: "Please enter valid email",
                    },
                    password:{  
                        required:  "Please enter password",
                    },
                }
            });
        }
    });
</script>
@endsection