@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
            <style>
                .poppins-light {
        font-family: "Poppins", sans-serif;
        font-weight: 300;
        font-style: normal;
    }

    .poppins-regular {
        font-family: "Poppins", sans-serif;
        font-weight: 400;
        font-style: normal;
    }

    .poppins-medium {
        font-family: "Poppins", sans-serif;
        font-weight: 500;
        font-style: normal;
    }

    .poppins-semibold {
        font-family: "Poppins", sans-serif;
        font-weight: 600;
        font-style: normal;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: "Poppins", sans-serif;
        font-weight: 500;
        font-style: normal;
    }

    .form-header {
        text-align: center;
        color: #ff6f5f;
        margin-top: 2rem;
    }

    .form-sub-header {
        text-align: center;
        color: rgb(158, 158, 158) !important;
        font-size: 15px;
        margin-bottom: 50px;
    }

    .form-section {

        width: 100%;
        max-width: 800px;
        height: auto;
        margin: 0px auto;
        padding: 10px 25px;
        border: 1px solid rgb(238, 238, 238);
        border-radius: 10px;
        margin-bottom:75px;

    }

    .form-right-sc {
        padding: 40px 25px;
        background-color: #f9f9f9;
        border-radius: 10px;

    }

    .price-select {
        font-size: 17px;
        line-height: 2;
    }

    .form-right-section {
        background-color: rgb(250, 250, 250);
    }

    a.ekhatabook-partner-code {
        color: #ff6f5f;
        text-decoration: none;
        font-weight: bold;
        font-size: 13px;
    }

    .optionl {
        color: gray;
    }

    .total {
        display: flex;
        justify-content: space-between;
        margin-top: 15px;
        border-bottom: 1px solid rgb(238, 238, 238);
        margin-bottom: 10px;

    }

    .total h5 {
        font-size: 17px;
    }

    .condition-apply {
        font-size: 14px;
    }

    .form-select {
        border: 1px solid #ff6f5f;
    }

    .btn-custom {
        background-color: #ff6f5f;
        width: 100%;
        border-radius: 10px;
        color: #fff;
    }

    .form-control {
        border: 1px solid #ff6f5f;
        border-radius: 10px;
        font-size: 14px;
        height: 42px;
    }

    .form-control:focus {
        color: var(--bs-body-color);
        background-color: var(--bs-body-bg);
        border-color: #ff6f5f;
        outline: 0;
        box-shadow: none;
    }

    .btn-custom:hover {

        color: #fff;
    }

    .form-left-sc {
        padding: 25px 15px;
    }

    .p-one {
        color: rgb(158, 158, 158);
        font-size: 15px;
        margin-top: 10px;
    }

    .p-two {
        color: rgb(56, 56, 56);
        font-size: 16px;
    }



    .p-three ul {
        padding: 0;
        margin: 0;
        list-style: none;
        padding-left: 20px;

    }

    .p-three ul li {
        color: #5f5f5f;
        font-weight: 400;
        font-size: 13px;
        margin-bottom: 7px;
        position: relative;
        padding-left: 30px;

    }

    .p-three ul li::before {
        content: '';
        position: absolute;
        left: 0;
        top: 50%;
        transform: translateY(-50%);
        width: 15px;
        height: 15px;
        background-image: url('{{ asset('images/check.png') }}');
        background-size: cover;
    }


    .gst {
        background-color: lightgreen;
        font-weight: bold;
        padding: 2px 8px;
        font-size: 11px;
        border-radius: 10px;
    }

    .price-cut {
        font-size: 13px;
        padding-right: 10px;
        color: rgb(158, 158, 158);
        text-decoration: line-through;
    }
            </style>
@endsection

@section('content')
    <section id="responsive-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="container">
                        <h4 class="form-header"> Upgrade to our premium plan </h4>
                        <p class="form-sub-header"> For continued access to EKhatabook on desktop, please buy our premium plan </p>
                
                        <div class="form-section">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-left-sc">
                                        <img src="{{asset('images/new_logo1.jpeg')}}" width="170px" style="margin-left: -16px;">
                
                                        <p class="p-two">What's included: </p>
                
                                        <div class="p-three">
                                            <ul>
                                                <li id="content"></li>
                                            </ul>
                                        </div>
                                    </div>
                
                                </div>
                
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-right-sc">
                                        <form action="{{url('buyPlan')}}" method="Post">
                                            @csrf
                                            <div class="row mb-3">
                                                <div class="col-md-9">
                                                    <select class="form-control" id="plan_id" name="plan_id">
                                                        @foreach ($plans as $plan)
                                                                <option value="{{$plan->plan_id}}">{{$plan->name}} ({{$plan->year}})</option>
                                                        @endforeach 
                                                    </select>
                                                </div>
                                            </div>
                
                                            <div class="total">
                                                <h5>Total</h5>
                                                <h5 >₹<span id="planAmount">2,499 </span> + 18% Tax</h5>
                                            </div>
                
                                            <p class="condition-apply">Include All <span class="gst">GST</span> and Terms And Condition
                                                Apply</p>
                
                                            <button type="submit" class="btn btn-custom">Buy Now</button>
                
                
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Responsive Datatable -->

   
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script>

$( document ).ready(function() {    
    getPlan();  
    $(document).on('change', '#plan_id', function (e ) {
        getPlan();
    }); 
});

   function getPlan() {
        var endpoint = base_url+'/plan/getData';
        var token = $("input[name='_token']").val();
        $.ajax({
                url: endpoint,
                method: 'POST',
                data: {
                    '_token': token,
                    'id': $('#plan_id').val(),
                },
                dataType: "json",
                success: function (data) {
                    if(data.title == 'Error'){
                        $('#loader').hide();
                        toastr.error(data.message, data.title);
                    }else{
                        $('#plan-price').html(data.data.amount);
                        $('#planAmount').html(data.data.amount);
                        $('#content').html(data.data.contents);
                    }
                }
            })
   }
   </script>
@endsection


