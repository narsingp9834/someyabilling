@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
           
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="jquery-val-form" method="POST" action="{{url('staff/save')}}" autocomplete="off">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">{{__("labels.add")}} Staff</h5>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="">Name</label>
                                        <input type="text" name="user_fname" id="user_fname" class="form-control" placeholder="Name" required> 
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="for" class="">Phone No</label>
                                        <input type="text" name="user_phone_no" id="user_phone_no" class="form-control" placeholder="Phone No">
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    
                                    <div class="form-group">
                                        <div class="d-flex justify-content-between">
                                        <label for="login-password">Password</label>
                                        </div>
                                        <div class="input-group input-group-merge form-password-toggle">
                                        <input type="password" class="form-control form-control-merge" id="password" name="password" tabindex="2" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" required="" minlength="8" maxlength="30" />
                                        <div class="input-group-append">
                                            <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                        </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary" >{{__("labels.submit")}}</button><a href="{{url('product')}}"> <button type="button" class="btn btn-secondary" >{{__("labels.cancel")}}</button></a>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /Validation -->
@endsection

