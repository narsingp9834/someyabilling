<!doctype html>
<html lang="en">

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home|SASTA AUR SECURE HAI BOSS
        APNA DIGITAL EKHATABOOK</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/logo.jpeg')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style Link -->
    <link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<style>
@media only screen and (max-width: 576px) {
    .navbar-custom .nav-link {
   
        color: #b7b7b7;
    border-bottom: 1px solid #5e5e5e;
   
}
.custom-btn{

    margin-top:15px;
    background-color: #ff4343;
    border-radius: 5px;
    padding: 7px 15px;
    font-size: 15px;
    color: #fff;
    margin-right: 5px;
    font-size:13px;

}
  }

  .close-btnn{
    font-size: 21px;
    color:#b7b7b7;
  }

  a{
    text-decoration: none;
    
  }
  .heading-main a{
    color:#ff4343;
    border:2px dotted #ff4343;
    padding:5px 12px;
    margin-bottom:25px

  }

  .tab-cousres-card .span-badge {
    background-color: #ffaa46 !important;
    color: #000000 !important;
    border-radius:5px;
    
}

.cousres-banner-card {
    background-color: #ff434314;
    border-radius: 25px;
    padding: 50px 45px;
    width: 100%;
    max-width: 950px;
    margin: auto;
    margin-bottom: 5rem;
    margin-top: 2rem;
    border-bottom: 3px solid #ff4343;
}

.btn-custom-ss {
    background-color:#ff4343;
    color: #fff;
    font-size: 16px;
    padding: 9px 20px;
    border-radius: 4px;
    font-family: 'Poppins', sans-serif !important;
    transition: all .5s ease;
    box-shadow: 0 0.5rem 1.125rem -0.5rem rgba(162, 105, 254, 0.9);
}
</style>

<body>
    @include('common.header')
     <!---======================Banner Start Here =====================================-->
<div class="container-fluid " style="margin-top:5rem">
<div id="carouselExample" class="carousel slide">
        <div class="carousel-inner">
        @foreach ($banners as $banner)
            <div class="carousel-item active">
                <img src="{{$banner->image}}" class="d-block w-100" alt="...">
            </div>
        @endforeach
        </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
            </button>
</div>

</div>
      <!---====================== Banner End Here=====================================-->
    
    

 

    <!--=========================== ===============================-->
    @foreach ($categories as $category)
        <div class="container" style="margin-top: 4rem;margin-bottom: 70px;">
            <h4 class="home-page-main-heading heading-main my-3"><a href="{{url('category/products/' . $category->category_id)}}">{{$category->name}}</a></h4>
            
                <div class="tab-content" id="pills-tabContent">
                    <!-- Mobile Application-->
                    <div class="tab-pane fade show active" id="mobile-application" role="tabpanel" tabindex="0">
                        <div class="row mt-5">

                        
                                @foreach ($category->products as $product)
                                
                                    <div class="col-lg-3 col-md-3">
                                        <a href="{{url('cms-product/view/' . $product->cms_product_id)}}" class="courses-a">
                                            <div class="tab-cousres-card" style="
                                            height: 320px;
                                        ">
                                                <div class="tab-cousres-card-img">
                                                    <img src="{{$product->image1}}" width="100%" style="height: 150px;" class="img-fluid tab-cousres-card-img-rounded">
                                                </div>
                                                <a href="{{url('cms-product/view/' . $product->cms_product_id)}}"><span class="span-badge">{{$product->name}}</span></a>
                                                <h6 class="tab-cousres-card-heading">{{ substr($product->contents, 0, 100) }}</h6>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            
                        </div>
                    </div>
                </div>
            
        </div>
    @endforeach 

    <div class="container">
        <div class="cousres-banner-card">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <div class="cousres-banner-card-left">
                        <h2>Let’s find the right <br> Web Solutions for you!</h2>
                        <p>EKHATABOOK mission is to develop easy and friedly An entrepreneur Solution. "Every day is a chance to begin again. Don't focus on the failures of yesterday, start today with positive thoughts and expectations."</p>
                        <a href="{{url('seller/login')}}" class="btn btn-custom-ss" style="margin-bottom: 15px;">Start Billing</a><br>
                    </div>
                </div>

                <div class="col-lg- col-md-5 col-sm-5">
                    <div class="cousres-banner-card-left">
                        <img src="{{asset('images/banner-bottom.jpg')}}" class="img-fluid" style="border-radius: 25px;">
                    </div>
                </div>

            </div>



        </div>
    </div>

    @include('common.footer')

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>