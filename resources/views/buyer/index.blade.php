@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap4.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection
 
@section('page-style')
<style>
    .loan-emi-calculator-sc {
        background-color: #fff;
        /* border: 1px solid #dee2e6; */
        width: 100%;
        max-width: 370px;
        margin: 0 auto;
        height: auto;
        padding: 0px 0px;
        margin-bottom: 50px;
        margin-top: 10px;
        padding-bottom:15px;

    }


    .header-heading {
        font-weight: bold;
        text-align: center;
        font-size: 19px;
        margin-bottom: 17px;
        padding-top: 15px;
    }

    .header-heading-span {
        color: #ff6f5f;
    }

    .loan-emi-calculator-body {
        border-radius: 10px;
        padding: 1px 5px;
        background-color: #fff;
        border-bottom: 1px solid #0000;
        box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 5px 0px, rgba(0, 0, 0, 0.1) 0px 0px 1px 0px;
    }

    .monthly-emi-heading {
        text-align: center;
        color: #ff6f5f;
        font-size: 14px;
        padding-top: 10px;
    }

    .price {
        text-align: center;
        color: #ff6f5f;
        font-size: 25px;
        margin-top: -12px;
        font-weight: bold;
    }

    .loan-emi-calculator-body-sc {

        padding: 0px 5px;

    }

    .bottom {
        border-bottom: 1px solid #eeeeee;
        padding: 0px 5px;
    }

    .m-main-heading {
        color: black;
        margin-bottom: 2px;
        margin-top: 9px;
        font-size: 14px;
        padding-left:10px;

    }

    .m-sub-heading {
        font-weight: bold;
        text-align: end;
        margin-top: -20px;
        margin-bottom: 7px;
        font-size: 14px;
        padding-right: 10px;

    }

    .total-payment {
        font-weight: bold;
    }

    .total-payment-sc {
        color: #ff6f5f !important;
    }

    .bottom-dotted {
        border-bottom: 2px dotted #eeeeee;
    }
</style>
@endsection

@section('content')
    <section id="responsive-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header border-bottom">
                        <h4 class="card-title">Buyer List</h4>
                        <div class="dt-action-buttons text-right">
                            <div class="dt-buttons d-inline-flex">
                                <a href="{{url('buyer/create')}}"><button class="btn btn-success mr-1 exportCustomer">Add Buyer</button></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-datatable">
                        <table class="dt-responsive table" id="table_buyer">
                            <thead>
                            <tr>
                                <th>{{__("labels.no")}}</th>
                                <th>Firm</th>
                                <th>GST</th>
                                <th>Phone</th>
                                <th>State</th>
                                <th>City</th>
                                <th>Id</th>
                                <th>{{__("labels.action")}}</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/ Responsive Datatable -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <section class="loan-emi-calculator-sc"> 
                    {{-- <div class="loan-emi-calculator-header">
                        <h3 class="header-heading"> Welcome  <span class="header-heading-span"> E- Store Solutions</span></h3>
                    </div>
         --}}
                    <div class="loan-emi-calculator-body">
                        <p class="monthly-emi-heading">Total Purchase</p>
                        <h2 class="price" id="totalPrice">₹ 10,0000</h2>
                        <div class="bottom"></div>
                        <br>
                        <div class="row">
                            <div class="col-5">
                                <div id="piechart_3d"></div>
                            </div>
                            <div class="col-7">
                                <div class="d-flex pp" style="justify-content: space-between;">
                                    <p><i class="fa fa-circle font-color"></i> Total Score</p>
                                    <h6>70%</h6>
        
                                </div>
        
                                <div class="d-flex pp" style="justify-content: space-between;">
                                    <p><i class="fa fa-circle font-color-sc"></i> Credit Amount</p>
                                    <h6 id="creditAmount">18000</h6>
        
                                </div>
        
                            </div>
                        </div>
                    </div>
    
                    <div class="bottom"></div>
        
                    <div class="loan-emi-calculator-body-sc">
                        
                        <p class="m-main-heading" id="femi_date">15-04-2024</p>
                        <h6 class="m-sub-heading emi">₹6500 </h6>
                    </div>
                    <div class="bottom"></div>
        
                    <div class="loan-emi-calculator-body-sc">
                        <p class="m-main-heading semi_date">10-03-2024</p>
                        <h6 class="m-sub-heading emi">₹6500 </h6>
                    </div>
                    <div class="bottom"></div>
                    <div class="loan-emi-calculator-body-sc">
                        <p class="m-main-heading temi_date">25-03-2024</p>
                        <h6 class="m-sub-heading emi">₹7000 </h6>
                    </div>
        
                   
                    <div class="bottom-dotted"></div>
        
                    <div class="loan-emi-calculator-body-sc">
                        <p class="m-main-heading total-payment">Total Payment</p>
                        <h6 class="m-sub-heading total-payment-sc" id="totalEMI">₹20000/- </h6>
                    </div>
                </section>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap4.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    <script src="{{ asset('js/buyer.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection


