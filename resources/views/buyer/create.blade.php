@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
           
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="jquery-val-form" method="POST" action="{{url('buyer/save')}}" autocomplete="off">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">{{__("labels.add")}} Buyer</h5>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="">Firm Name</label>
                                        <input type="text" name="user_fname" id="user_fname" class="form-control" placeholder="Firm Name" required> 
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="for" class="">GST No</label>
                                        <input type="text" name="gst_no" id="gst_no" class="form-control" placeholder="GST No">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="for" class="">Phone No</label>
                                        <input type="text" class="form-control" name="user_phone_no" id="user_phone_no"
                                            placeholder="Phone No" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="for" class="">Target</label>
                                        <input type="text" class="form-control" name="target"
                                            placeholder="Enter Target" required>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="for" class="">State</label>
                                        <select class="form-control" id="state_id" name="state_id" class="form-control"> 
                                            <option value="">Select State</option>
                                            @foreach ($states as $state)
                                                <option value="{{$state->state_id}}">{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="for" class="">City</label>
                                        <select class="form-control" id="city_id" name="city_id" class="form-control"> 
                                            <option value="">Select City</option>
                                        </select>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="for" class="">Pin No</label>
                                        <input type="text" class="form-control" name="pin_no"
                                            placeholder="Enter Pin No" required>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="for" class="">Passsword</label>
                                        <div class="input-group input-group-merge form-password-toggle">
                                            <input type="password" class="form-control form-control-merge" id="password" name="password" tabindex="2" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" required="" minlength="8" maxlength="30" />
                                            <div class="input-group-append">
                                                <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="for" class="">Address</label>
                                        <textarea class="form-control" name="address" id="address" required>Enter Address
                                        </textarea>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary" >{{__("labels.submit")}}</button><a href="{{url('product')}}"> <button type="button" class="btn btn-secondary" >{{__("labels.cancel")}}</button></a>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /Validation -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection

@section('page-script')
    <script src="{{ asset('js/buyer.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection