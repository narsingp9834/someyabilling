@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap.min.css')) }}">
@endsection
@section('page-style')
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">

    <style>
            .span-design {

            margin-top: -15px;


            }
           

            .dashboard {
            font-size: 25px;
            }

            .inner p {
            font-size: 18px;
            margin-top: -10px;
            }

            .inner h3 {
            font-size: 25px !important;
            }

            .small-box {
                border-radius: .25rem;
                box-shadow: 0 0 1px rgba(0,0,0,.125), 0 1px 3px rgba(0,0,0,.2);
                display: block;
                margin-bottom: 20px;
                position: relative;
            }
            .bg-info {
                background-color: #17a2b8 !important;
            }
            .bg-info, .bg-info>a {
                color: #fff !important;
            }
            .small-box>.inner {
                padding: 10px;
            }
            .small-box .icon {
                color: rgba(0, 0, 0, .15);
                z-index: 0;
            }

            .small-box>.small-box-footer {
                background-color: rgba(0, 0, 0, .1);
                color: rgba(255, 255, 255, .8);
                display: block;
                padding: 3px 0;
                position: relative;
                text-align: center;
                text-decoration: none;
                z-index: 10; 
            }
            .time-sc{
                float:right;margin-top:-55px;background-color:#ed3240;color:#fff;padding:4px 10px;border-radius:4px
            }
    </style>
@endsection

@section('content')
    <input type="hidden" id="rescheduleMsg" value="{{Config::get('messages.reschedule_popup')}}">
    <input type="hidden" id="role" value="{{Auth::user()->user_type}}">
    <!-- Dashboard Analytics Start -->

    @if (Auth::user()->user_type == 'B')
        

    <div class="content-wrapper ">
    
      
      
      <div class="content-body">
      <div class="row mt-5">

<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$purchaseTotal[0]->total ?? '0'}}/-</h3>
            <p>Purchase</p>
        </div>
        <div class="icon">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i>

        </div>
        <a href="{{url('supplier/list')}}" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$staffTotal ?? '0'}}</h3>
            <p>Staff</p>
        </div>
        <div class="icon">
            <i class="fa fa-users" aria-hidden="true"></i>

        </div>
        <a href="{{url('staff')}}" class="small-box-footer">More info<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$productTotal ?? '0'}}</h3>
            <p>Product</p>
        </div>
        <div class="icon">
            <i class="fa fa-briefcase" aria-hidden="true"></i>

        </div>
        <a href="{{url('product')}}" class="small-box-footer">More info<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$buyerTotal ?? '0'}}</h3>
            <p>Buyer</p>
        </div>
        <div class="icon">
            <i class="fa fa-user" aria-hidden="true"></i>

        </div>
        <a href="{{url('buyer')}}" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$invoiceTotal[0]->total ?? '0'}}/-</h3>
            <p>Invoice</p>
        </div>
        <div class="icon">
            <i class="fa fa-file" aria-hidden="true"></i>
        </div>
        <a href="{{url('invoice/list')}}" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$quptationTotal[0]->total ?? '0'}}/-</h3>
            <p>Quotation</p>
        </div>

        <div class="icon">
            <i class="fa fa-file" aria-hidden="true"></i>
        </div>

        <a href="{{url('quotation')}}" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$chalanTotal[0]->total ?? '0'}}</h3>
            <p>Delivery Chalan</p>
        </div>
        <div class="icon">
            <i class="fa fa-file" aria-hidden="true"></i>
        </div>
        <a href="{{url('chalan')}}" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$invoiceTotal[0]->total ?? '0'}}/-</h3>
            <p>Sale Report</p>
        </div>
        <div class="icon">
            <i class="fa fa-flag" aria-hidden="true"></i>

        </div>
        <a href="#" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>

<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>25K to 1Lakh </h3>
            <p>Loan Offer</p>
        </div>
        <div class="icon">
            <i class="fa fa-credit-card" aria-hidden="true"></i>


        </div>
        <a href="{{url('buyer-report')}}" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>

<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$invoiceTotal[0]->total ?? '0'}}/-</h3>
            <p>GSTR - 1</p>
        </div>
        <div class="icon">
            <i class="fa fa-file" aria-hidden="true"></i>


        </div>
        <a href="{{url('invoice/list/sell')}}" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$purchaseTotal[0]->total ?? '0'}}/-</h3>
            <p>GSTR -2</p>
        </div>
        <div class="icon">
            <i class="fa fa-file" aria-hidden="true"></i>

        </div>
        <a href="{{url('supplier/list/supplier')}}" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$invoiceTotal[0]->total - $purchaseTotal[0]->total}}</h3>
            <p>GSTR - 3B</p>
        </div>
        <div class="icon">
            <i class="fa fa-file" aria-hidden="true"></i>

        </div>
        <a href="{{url('gstr3')}}" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$activePlan ?? '0'}}</h3>
            <p>Premium Plan</p>
        </div>
        <div class="icon">
            <i class="fa fa-calendar" aria-hidden="true"></i>


        </div>
        <a href="{{url('seller-plan')}}" class="small-box-footer">More info<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$requestPlan ?? '0'}}</h3>
            <p>Service Order</p>
        </div>
        <div class="icon">
            <i class="fa fa-calendar" aria-hidden="true"></i>

        </div>
        <a href="#" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$units ?? '0'}}</h3>
            <p>Create Units</p>
        </div>
        <div class="icon">
            <i class="fa fa-envelope" aria-hidden="true"></i>

        </div>
        <a href="#" class="small-box-footer">More info <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-6">

    <div class="small-box bg-info">
        <div class="inner">
            <h3>{{$hsns ?? '0'}}</h3>
            <p>Create HSN</p>
        </div>
        <div class="icon">
            <i class="ion ion-bag"></i>
        </div>
        <a href="#" class="small-box-footer">More info<svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><circle cx="12" cy="12" r="10"></circle><polyline points="12 16 16 12 12 8"></polyline><line x1="8" y1="12" x2="16" y2="12"></line></svg></a>
    </div>
</div>





</div>
            
  
      </div>
    </div>
    @endif
   
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/js/all.min.js" integrity="sha512-f6ARr8ILY9YDtYq8Kpl3je9bFh1xvqTtDKA+jZL5OxYbQ7HhEFL8BYY3M2eJrLb/6WvSvE8xGnXJpZQLaJSv5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="{{ asset('js/dashboard.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection
