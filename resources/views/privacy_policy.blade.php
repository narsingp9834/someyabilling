<!doctype html>
<html lang="en">

<head>
    <!-- Meta Tag -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Privacy Policy|SASTA AUR SECURE HAI BOSS
        APNA DIGITAL EKHATABOOK</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/logo.jpeg')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom Style Link -->
    <link rel="stylesheet" href="{{asset('assets/css/custom-style.css')}}">
    <!-- Bootstrap Icon -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <!--Font Awsome Icon -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    @include('common.header')
     <!---======================Banner Start Here =====================================-->

      <!---====================== Banner End Here=====================================-->
    
        

 

        <div class="container" style="margin-top: 6rem;">
            <h4 class="home-page-main-heading" style="color: #ff4343; padding: 5px 12px; margin-bottom: 25px;">Privay Policy</h4>
            
          
                <p>We are responsible to secure your privacy thats-why E- Store Solutions created this privacy policy. E- Store Solutions Privacy Policy explains nature of information that we collect, why and how we collect, how we share and manage the contents you place in our products and services.
</p><h6> 1. Scope of Privacy Policy:</h6>
            <p>The Privacy Policy applies to the informations which we obtain from your use of “E- Store Solutions” or in connection with the use of our hosted or Mobile Software application or Support Services via a “Device” or “Website” when you interact with Ekhatabook. Ekhatabook website may contain links to other Websites. The content of such website along with information gathered is governed by privacy statement of specific websites. Ekhatabook recommends you to have a view on the privacy policy of such websites to understand their information practices.
            “Ekhatabook Services” includes
            •	Websites
            •	SaaS Products.
            •	Downloadable Products.
            except
            Ekhatabook Products or services for which a separate Privacy is provided.
            Third party products. Third Party products or services are those, you choose to integrate with Ekhatabook products. Ekhatabook encourages you to review the policies of third party products and services and to make sure that you are comfortable with the ways in which they collect and use your information.
            A “Device” is any gadget used to access the Ekhatabook Services,such as a desktop, laptop, mobile phone, tablet, or other consumer electronic device with no limitation. Unless otherwise stated, our SaaS Products and our Downloadable Products are treated the same for the purposes of this document.
            By registering for or using Ekhatabook Services, you consent to the collection, transfer, processing, storage, disclosure and other uses described in this Privacy Policy.
</p>
            <h6>2. Definition</h6>
            <p>Ekhatabook : E- Store Solutions.
            Ekhatabook Services : Ekhatabook websites, SaaS Products, Downloadable products.
            Downloadable Products : Downloadable software products and mobile applications Provided by Ekhatabook.
            SaaS Products : Ekhatabook’s “Cloud” hosted solutions also Ekhatabook hosted solutions that display a link to this Privacy Policy.
            Websites : Ekhatabook’s websites including outputbooks.com.
            Content : Every data and information you submit, upload, post, create, transmit, store or display in Ekhatabook Services.
            Information : Data, content or information collected by Ekhatabook.</p>
            
            <h6> 3. Updates to our Privacy Policy</h6>
           <p> The Privacy Policy may change occasionally. If we make any changes, we will notify you by revising the “Effective Starting date” at the top of this privacy policy and in some cases, we may provide you with an additional notification (adding a statement to login screen or by an email notification). We recommend you to review our Privacy Policy when you use Ekhatabook Services to be informed about our information practices. If you deny with any changes to this Privacy Policy, you will need to stop using Ekhatabook Services and also have to deactivate your account(s).
</p>
            <h6> 4. Information we gather</h6>
            <p>Information you provide to us: When you register for and use Ekhatabook, you provide the information such as name, Email address, phone number and country.
            Information from your use of our service: The information we gather while you using our service such as IP address, location information, date and time, browser type etc.
            Cookies : We store “cookies”, – a string of code on your computer, when your use Ekhatabook services. These cookies are used to gather information about when you visit our services, when you use our service, your browser, your operating system and other information. There may be option for you to block or delete cookies on most browser. If you block or delete cookies, our service may not work properly.
            Third Party Cookies : The utilization of cookies by third party is not protected by our Privacy Policy. We cannot control or access over these cookies. To make you to navigate our website easily, third parties use session ID cookies. We use third-party services to provide us with necessary hardware, software, networking, storage, and related technology required to run and improve our services.
            Google Analytics : Google Analytics collects information about your visiting frequency to this site, the pages you visit, how you came to this site along with referal sites (sites which you refer to come to our sites). We gather this information only to improve our services and website.
            Google Analytics collects only the IP address, also it will set you a permanent-unique cookie to identify you on next visit to our site. This cookies can’t be used by anyone except Google, but it is restricted to use or share your information as per Google Analytics Terms of Use and the Google Privacy Policy.You can disable the cookie, if you wish.
</p>
            <h6>5. How we use your Information</h6>
            <p>
            •	To enable you to use Ekhatabook Services .
            •	To provide, maintain, improve and promote Ekhatabook services.
            •	To provide customer services.
            •	To communicate with you, including responding your queries, comments and requests, give you the information about the services and features.
            •	To better understand your needs and interests.
            •	To analyze trends, usage, and activities in connection with Ekhatabook Services.
            •	To prevent fraudulent transaction, unauthorized and illegal transaction to Ekhatabook Services.
            •	To transfer your information during the time of sales.
            •	To notify you, for sending updates and alerts.
            •	If you didn’t wish to receive our newsletters, you may un-subscribe by following the link located within our emails.
            
            Log Data : We want to inform you that whenever you use our Service, in case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your devices’s Internet Protocol (“IP”) address, device name, operating system version, configuration of the app when utilising our Service, the time and date of your use of the Service, and other statistics.
            Information Collection and Use : For a better experience while using our Service, we may require you to provide us with certain personally identifiable information, including but not limited to users name, address, location & pictures. Our Software may access your access your contact list, photos, device state and microphone. The information that we request is retained on your device and is not collected by us in any way.
</p>
            <h6>6. Share or disclosure of your information</h6>
            <p>we never share your personal information with any other third parties except that we specified under this policy. Inorder to provide service to you, we will share your information with our business partners and service providers. In case of sharing any information with third parties, we surely will get prior permission from you. We are tracking you, when you visit our site inorder to collect some information about your online activities to improve the product as we have use some API which use cookies and some technologies. We may transfer your information if required, during negotiation of, reorganization, merger, or sales of the company. In addition, we may disclose your personal information to law enforcement authorities if its seems to be necessary and also to protect our users, employees, or the generic public.
            We won’t disclose or share your Personal Information or Content with third parties except described in our Privacy Policy. Also we won’t sell your Personal Information or Content.
</p>
            <h6> 7. Third Parties and Public Information</h6>
            <p>Our (Ekhatabook) Blog : On our Website, we have many public blogs. Any Information that you provide here may be read, gathered, and can be used by anyone. If you like to take away the personal information that appears on our blog, you can intimate it. We provide it along with this privacy policy (Contact information).
            We may publish customer testimonials if you wish, which may contain your personal information. We also get permission to post customer’s name along with their testimony. For removing any personal information from a testimony, please intimate us. We provide it along with this privacy policy (Contact information).
            Information in Third Parties : You may use the third-party widget, we are not responsible for the information submitted to them and our Privacy Policy is not cover your use of any third-party widget, features, websites, links to their websites or services. We recommend you to read the Privacy Policy of third-party website carefully before using it.
            Social Media Widget : Social media widgets are also included in our Websites. These features collect your IP address, your page navigation information, and may set cookies to monitor the proper function.
</p>
            <h6> 8. Update your information</h6>
            <p>If you want to add or remove or update any information you can email us. We will surely respond you. Also we won’t respond unreasonable request. In case of updating information in third party, we will redirect it to them, we are not its responsibility.
            <p>
            <h6> 9. Security</h6>
           <p> The security of your personal information and data are very important to Ekhatabook. We always take precautions to safeguard your informations. Your informations are stored on secured server in a controlled environment. The sensitive information you enter is encrypted and transmitted over the network.
            As Internet is an Open Global Communication Vehicle, we cannot guarantee that the transmission of information over Internet 100% safe from intrusion(hacking) . Also there will be no guarantee for the passively-collected information you choose to store in SaaS products are at a protected level.
</p>
            <h6> 10. Contact Information</h6>
           <p> If you have any queries regarding Ekhatabook Privacy Policy, please contact legal@Ekhatabook.com . We are here to clarify you .
            
</p>
        </div>
    

    @include('common.footer')

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>