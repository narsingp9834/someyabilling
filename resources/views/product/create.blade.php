@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
    {{-- Vendor Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel='stylesheet' href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">
           
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="jquery-val-form" method="POST" action="{{url('product/save')}}" autocomplete="off">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">{{__("labels.add")}} Product</h5>
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-lg-4 col-md-3  col-12">
                                    <div class="form-group">
                                        <label id="">Date</label>
                                        <input type="text" name="date" id="date" class="form-control" required> 
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-3  col-12">
                                    <div class="form-group">
                                        <label id="">Product Name</label>
                                        <input type="text" name="product_name" id="product_name" class="form-control" placeholder="Product Name" required> 
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-3  col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Unit</label>
                                        <select class="form-control" name="unit_id" id="unit_id" required>
                                                <option value="">Select Unit</option>
                                                @foreach ($units as $unit)
                                                        <option value="{{$unit->unit_id}}">{{$unit->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-3  col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Max Quantity</label>
                                        <input type="text" class="form-control" name="max_quantity"
                                            placeholder="Enter Max Quantity" required>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-3  col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Min Quantity</label>
                                        <input type="text" class="form-control" name="min_quantity"
                                            placeholder="Enter Min Quantity" required>
                                    </div>

                                </div>

                                <div class="col-lg-4 col-md-3  col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Purchase Price</label>
                                        <input type="text" class="form-control" name="purchase_price"
                                            placeholder="Enter Purchase Price" required>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-3  col-12">
                                    <div class="form-group">
                                        <label id="for" class="">Sale Price</label>
                                        <input type="text" class="form-control" name="sale_price"
                                            placeholder="Enter Sale Price" required>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-3  col-12">
                                    <div class="form-group">
                                        <label id="for" class="">HSN</label>
                                        <select class="form-control" name="hsn_id" id="hsn_id" required>
                                                <option value="">Select HSN</option>
                                                @foreach ($hsns as $hsn)
                                                        <option value="{{$hsn->hsn_id}}">{{$hsn->code}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 ">
                                    <button type="submit" class="btn btn-primary" >{{__("labels.submit")}}</button><a href="{{url('product')}}"> <button type="button" class="btn btn-secondary" >{{__("labels.cancel")}}</button></a>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- /Validation -->
@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>

@endsection

@section('page-script')
    <script src="{{ asset('js/product.js') }}?v={{Config::get('constants.portal_version')}}"></script>
@endsection