<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceProduct extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = [
        'invoice_id',
        'product_id',
        'quantity',
        'price',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public $primaryKey = 'invoice_product_id';

    public function getProduct() {
        return $this->hasOne(Product::class,'product_id', 'product_id')->with('unit');
    }
}
