<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    public $primaryKey = 'supplier_id';

    protected $fillable = [
        'seller_id',
    	'supplier_name',
        'phone_no',
        'invoice_no',
        'date',
        'gst_no',
        'product',
        'hsn_code',
        'price',
        'quantity',
        'igst',
        'cgst',
        'sgst',
        'total',
        'payment_mode',
        'state_id'
    ];
    public $timestamps = false;
}
