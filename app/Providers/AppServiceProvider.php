<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Config;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

use App\Models\NavigationMenu;
use App\Models\OrganizationSetting;
use App\Models\Parameter;
use App\Models\UserRole;
use App\Models\Role;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (\Schema::hasTable('parameters')) {
            $system_parameters = DB::table('parameters')->get();
            if ($system_parameters && $system_parameters->isNotEmpty()){
                foreach ($system_parameters as $data) {
                    if($data->parameter_key == "mail_driver"){
                        $mail_driver = $data->parameter_value;
                    }elseif($data->parameter_key == "mail_host"){
                        $mail_host = $data->parameter_value;
                    }elseif($data->parameter_key == "mail_port"){
                        $mail_port = $data->parameter_value;
                    }elseif($data->parameter_key == "mail_username"){
                        $mail_username = $data->parameter_value;
                    }elseif($data->parameter_key == "mail_password"){
                        $mail_pwd = $data->parameter_value;
                    }elseif($data->parameter_key == "mail_encryption"){
                        $mail_encryption = $data->parameter_value;
                    }elseif($data->parameter_key == 'mail_from_address'){
                        $mail_from_address = $data->parameter_value;
                    }elseif($data->parameter_key == 'mail_from_name') {
                        $mail_from_name = $data->parameter_value;
                    }elseif($data->parameter_key == 'aws_access_key_id') {
                        $aws_access_key_id = $data->parameter_value;
                        Config::set('aws.credentials.key',$data->parameter_value);
                    }elseif($data->parameter_key == 'aws_secret_access_key') {
                        $aws_secret_access_key = $data->parameter_value;
                        Config::set('aws.credentials.secret',$data->parameter_value);
                    }elseif($data->parameter_key == 'aws_region') {
                        $aws_region = $data->parameter_value;
                        Config::set('aws.region',$data->parameter_value);
                    }elseif($data->parameter_key == 'aws_bucket') {
                        $aws_bucket = $data->parameter_value;
                    }else{
                        Config::set('constants.'.$data->parameter_key, $data->parameter_value);
                    }
                }

                $messageData = DB::table('messages')->get();
                if ($messageData && $messageData->isNotEmpty()){
                    foreach ($messageData as $message) {
                        Config::set('messages.'.$message->message_for, $message->message_details);
                    }
                }

                $aws = array(
                    'driver' => 's3',
                    'key'    => $aws_access_key_id??'',
                    'secret' => $aws_secret_access_key??'',
                    'region' => $aws_region??'',
                    'bucket' => $aws_bucket??''
                );

                $mailSetup = array(
                    'transport'     => $mail_driver,
                    'host'          => $mail_host??'',
                    'port'          => $mail_port??'',
                    'encryption'    => $mail_encryption,
                    'username'      => $mail_username??'',
                    'password'      => $mail_pwd??'',
                    'timeout'       => null,
                    'auth_mode'     => null,
                );
                $mail_form = array('address' => $mail_from_address??'', 'name' => $mail_from_name);

                // setup filesystem
                Config::set('filesystems.disks.s3', $aws);
                // setup mail
                Config::set('mail.mailers.smtp', $mailSetup);
                Config::set('mail.from', $mail_form);
            }
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function($view){
            $navigationMenus = $navigationSubMenus = array();
            view()->share(['menus' => $navigationMenus]);
        });
    }
}
