<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CommonMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */ 
    public function __construct($details=null,$user=null,$customer=null,$appointmentData=null,$other=null){
        $this->details = $details;
        $this->user    = $user;
        $this->customer    = $customer;
        $this->appointmentData    = $appointmentData;
        $this->other    = $other;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        $details = $this->details;
        $subject = $details['subject'];
        $user    =  $this->user;
        $customer =  $this->customer;
        $appointmentData =  $this->appointmentData;
        return  $this->subject($subject)->with(
            [
                'details' => $details, 
                'user'    => $user, 
                'customer'=> $customer,
                'appointmentData'=> $appointmentData,
                'other' =>  $this->other
            ]
        )->markdown('email.index');
    }
}
