<?php

namespace App\Http\Controllers;
use App\Models\Invoice;
use App\Models\InvoiceProduct;
use Illuminate\Http\Request;
use App\Traits\CommonTrait;
use DataTables;

class QuotationController extends Controller
{
    use CommonTrait;
    public function __construct()
    {
        $this->title = "Quotation";
        $this->middleware('auth');
    }

    public function index(Request $request) {

        try {
            if ($request->ajax()) {
                $sellerId = $this->getSellerId();
                $data = Invoice::join('buyers','buyers.buyer_id','invoices.buyer_id')    
                ->join('users','users.id','buyers.user_id')
                ->where('invoices.seller_id',$sellerId)
                ->where('invoice_type','Quotation');
                

                return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('status', function ($row) {
                    $id = encrypt($row->invoice_id);
                    if ($row->status == 'Active') {
                        $status = "<button title='Active' data-id='$id' data-type='Inactive' data-model='Invoice' data-field='status' class='btn btn-success status'>Active</button>";
                    } else {
                        $status = "<button title='Inactive' data-id='$id' data-type='Active' data-model='Invoice' data-field='status' class='btn btn-danger status'>Inactive</button>";
                    }
                    return $status;
                })
                ->addColumn('to_invoice', function ($row) {
                    $id = encrypt($row->invoice_id);
                    $btn = "<button data-id='$id' class='btn btn-success to_invoice'>Make Invoice</button>";
                    return $btn;
                })
                ->addColumn('action', function ($row) {
                    $id  = encrypt($row->invoice_id);
                    $btn = "<a href='".url('/invoice/download/'.$id)."' class='item-edit text-dark' title='Download Invoice'><svg viewBox='0 0 24 24' width='16' height='16' stroke='currentColor' stroke-width='2' fill='none' stroke-linecap='round' stroke-linejoin='round' class='css-i6dzq1'><path d='M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4'></path><polyline points='7 10 12 15 17 10'></polyline><line x1='12' y1='15' x2='12' y2='3'></line></svg></a>";

                    $btn .= "<a href='".url('/quotation/edit/'.$id)."' class='item-edit text-dark ' style='margin-left:2px;' title='Edit Invoice'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='Invoice' title='Delete Quotation'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->rawColumns(['action','status','to_invoice'])
                ->make(true);
            }

            $title = $this->title;
            return view('quotation.index',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('quotation/create');
        }
    }

    public function create() {
        $title = $this->title;

        $sellerId = $this->getSellerId();
        $buyers   = $this->getBuyers( $sellerId);
        $hsns     = $this->getHSNS( $sellerId);
        $products = $this->getProducts($sellerId);

        $invoice_no = null;

        return view('quotation.create',compact('title','products','buyers','hsns','invoice_no'));
    }

    public function store(Request $request) {
        try {
            $input = $request->all();
            $input['seller_id'] = $this->getSellerId();
            $input['invoice_type'] = 'Quotation';
            $invoice = Invoice::create($input);

            foreach ($input['columns'] as $key => $column) {
                $productData = array();
                $productData['invoice_id'] = $invoice->invoice_id;
                $productData['product_id']  = $column['product_id'];
                $productData['quantity']    = $column['quantity'];
                $productData['price']       = $column['price'];
                InvoiceProduct::create($productData);
            }
            toastr()->success("Quotation Created Successfully");
            return redirect('quotation');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('quotation/create');
        }
    }

    public function edit($invoiceId) {
        try {
            $title = $this->title;
            $sellerId = $this->getSellerId();
            $invoice = Invoice::find(decrypt($invoiceId));
            $invoice->load('products');
            $buyers   = $this->getBuyers( $sellerId);
            $hsns     = $this->getHSNS( $sellerId);
            $products = $this->getProducts($sellerId);
            return view('quotation.edit',compact('title','invoice','buyers', 'hsns','products'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('invoice');
        }
    }

    public function update(Request $request) {
        try {
            $input = $request->all();

            $searchInput['invoice_id'] = $input['invoice_id'];

            Invoice::updateOrcreate($searchInput, $input);

            foreach ($input['columns'] as $key => $column) {
                $productData = array();
                $productData['invoice_id'] = $input['invoice_id'];
                $productData['product_id']  = $column['product_id'];
                $productData['quantity']    = $column['quantity'];
                $productData['price']       = $column['price'];

                if(isset($column['invoice_product_id'])) {
                    $search['invoice_product_id'] = $column['invoice_product_id'];
                    InvoiceProduct::updateOrcreate($search,$productData);
                }else{
                    InvoiceProduct::create($productData);
                }
            }

            toastr()->success("Quotation Updated Successfully");
            return redirect('quotation');

        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('quotation');
        }
    }
}
