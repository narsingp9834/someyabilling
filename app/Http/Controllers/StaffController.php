<?php

namespace App\Http\Controllers;

use App\Models\Staff;
use App\Models\User;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Hash;

class StaffController extends Controller
{
    use CommonTrait;
    public function __construct()
    {
        $this->title = "Staff";
        $this->middleware('auth');
    }

    public function index(Request $request) {

        try {
            if ($request->ajax()) {
                $sellerId = $this->getSellerId();
                $data = Staff::join('users','users.id','staff.user_id')->where('seller_id',$sellerId)->whereNull('users.deleted_at');
                

                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) use ($request){
                    $id  = encrypt($row->id);
                    
                    $btn = "<a href='".url('/staff/edit/'.$id)."' class='item-edit text-dark' title='Edit Product'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";
                    

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='User' title='Delete Staff'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->addColumn('name', function ($row) {
                    return $row->user_fname . " ".$row->user_lname ;
                })
                ->rawColumns(['action'])
                ->make(true);
            }

            $title = $this->title;
            return view('staff.index',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('staff');
        }
    }

    public function create() {
        $title = $this->title;
        return view('staff.create',compact('title'));
    }

    public function store(Request $request) {
        try {
            $input = $request->all();
            $input['email'] = $input['user_phone_no'];
            $input['show_password'] = $input['password'];
            $input['password'] = Hash::make($input['password']);
            $input['user_type'] = 'S';
            $user =  User::create($input);

            $sInput['user_id'] = $user->id;
            $sInput['seller_id'] = $this->getSellerId();
            Staff::create($sInput);
            toastr()->success("Staff Created Successfully");
            return redirect('staff');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('staff/create');
        }
    }

    public function edit($userId) {
        try {
            $title = $this->title;
            $user = User::find(decrypt($userId));
            return view('staff.edit',compact('title','user'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('staff');
        }
    }

    public function update(Request $request) {
        try {
            $input = $request->all();

            $searchInput['id'] = $input['id'];
            $input['email'] = $input['user_phone_no'];
            User::updateOrcreate($searchInput, $input);
            toastr()->success("Staff Updated Successfully");
            return redirect('staff');

        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('staff');
        }
    }
}
