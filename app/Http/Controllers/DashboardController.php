<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Banner;
use App\Models\Buyer;
use App\Models\Category;
use App\Models\CMSProduct;
use App\Models\ContactUs;
use App\Models\HSN;
use App\Models\Invoice;
use App\Models\Plan;
use App\Models\Product;
use App\Models\Specialization;
use App\Models\Staff;
use App\Models\Supplier;
use App\Models\Unit;
use App\Models\User;
use App\Models\UserPlan;
use App\Models\UserPlanRequest;
use Illuminate\Http\Request;
use Auth;
use DataTables;
use DB;
use Illuminate\Support\Carbon;
use App\Traits\CommonTrait;
use Session;

class DashboardController extends Controller
{

	use CommonTrait;
	public function __construct()
  	{
	    // $this->middleware('auth');
	    // $this->middleware(function ($request, $next) {
	    //   $this->user = Auth::user();
	    //   return $next($request);
	    // });
	 }

	  // Dashboard
	public function dashboard()
	{
		try{
			// dd(Session::get('USER_TYPE'));
			$pageConfigs = ['pageHeader' => false];
			$data = [];

			if(Auth::user()->user_type == 'Buyer') {
				$buyer_id = Buyer::where('user_id',Auth::id())->value('buyer_id');
				$getCreditAmount = Invoice::select(DB::raw("MIN(invoice_products.price * invoice_products.quantity) as credit"),DB::raw("SUM(invoice_products.price * invoice_products.quantity) as totalValue"))->join('invoice_products','invoice_products.invoice_id','invoices.invoice_id')->where('buyer_id',$buyer_id)->get();

				$data['getCreditAmount'] = $getCreditAmount[0]->credit;
				$data['totalValue'] =  $getCreditAmount[0]->totalValue;
			}

			$sellerId = $this->getSellerId();

			$currentMonthName = date('F');

			$monthMap = [
                'January' => '01',
                'February' => '02',
                'March' => '03',
                'April' => '04',
                'May' => '05',
                'June' => '06',
                'July' => '07',
                'August' => '08',
                'September' => '09',
                'October' => '10',
                'November' => '11',
                'December' => '12',
            ];

			$numericMonth = $monthMap[$currentMonthName];

			$purchaseTotal = Supplier::select(DB::raw("SUM(suppliers.total)as total"))->where('seller_id',$sellerId)
			->where(DB::raw("MONTH(suppliers.date)"), '=', $numericMonth)
			->get();

			$staffTotal =Staff::join('users','users.id','staff.user_id')->where('seller_id',$sellerId)->whereNull('users.deleted_at')->count();

			$productTotal = Product::where('seller_id',$sellerId)->count();

			$buyerTotal = Buyer::where('seller_id',$sellerId)->count();

			$invoiceTotal = Invoice::select(DB::raw("SUM(grand_total)as total"))->where('seller_id',$sellerId)->where('seller_id',$sellerId)->where(DB::raw("MONTH(date)"), '=', $numericMonth)->where('invoice_type','Invoice')->get();

			$quptationTotal = Invoice::select(DB::raw("SUM(grand_total)as total"))->where('seller_id',$sellerId)->where('seller_id',$sellerId)->where(DB::raw("MONTH(date)"), '=', $numericMonth)->where('invoice_type','Quotation')->get();

			$chalanTotal = Invoice::select(DB::raw("SUM(grand_total)as total"))->where('seller_id',$sellerId)->where('seller_id',$sellerId)->where(DB::raw("MONTH(date)"), '=', $numericMonth)->where('invoice_type','Chalan')->get();

			$activePlan = UserPlan::where('user_id',Auth::id())->count();

			$requestPlan = UserPlanRequest::where('user_id',Auth::id())->where('status','Requested')->count();

			$units = Unit::where('seller_id',$sellerId)->count();

			$hsns = HSN::where('seller_id',$sellerId)->count();

			return view('/dashboard/dashboard', ['pageConfigs' => $pageConfigs, 'data' => $data, 'purchaseTotal'=> $purchaseTotal, 'staffTotal'=>$staffTotal, 'productTotal' => $productTotal, 'buyerTotal'=>$buyerTotal, 'invoiceTotal'=>$invoiceTotal, 'quptationTotal'=>$quptationTotal, 'chalanTotal' => $chalanTotal, 'activePlan'=>$activePlan, 'requestPlan'=>$requestPlan, 'units'=>$units , 'hsns'=>$hsns]);	
		}catch(\Exception $e){
			dd($e);
			toastr()->error(Config('messages.500'));
            return redirect('home');
		}
	}

	public function showHomePage() {
		$banners = Banner::all();

		$categories = Category::all();

		$productData = [];

		foreach ($categories as $key => $category) {
			$products = CMSProduct::where('category_id',$category->category_id)->limit(4)->get();
			$categories[$key]->products = $products;
		}
		// dd($categories[0]->products);
		return view('homepage',compact('banners','categories'));
	}

	public function sellerList(Request $request) {
		try {
            if ($request->ajax()) {
                $data = User::whereNotNull('phone_verified_at')->where('user_type','B');
                

                return Datatables::of($data)
                ->addIndexColumn()
				->addColumn('service_name', function ($row){
					return $this->getPlan($row->id);
				})
				->addColumn('amount', function ($row){
					$userPlanRequest = UserPlanRequest::where('user_id',$row->id)->first();
					if ($userPlanRequest != null) {
							$plan = Plan::find($userPlanRequest->plan_id);
							return $plan->amount;
					} else {
						return null;
					}
				})
				->addColumn('order_id', function ($row){
					$userPlanRequest = UserPlanRequest::where('user_id',$row->id)->first();
					if ($userPlanRequest != null) {
						return '240330SR'.$userPlanRequest->user_plan_request_id;
					} else {
						return null;
					}
				})
				->addColumn('status', function ($row){
					return $this->getPlanStatus($row->id);
				})
				->addColumn('duration', function ($row){
					return $this->getPlanDuration($row->id);
				})
                ->addColumn('action', function ($row){
                    $id  = encrypt($row->buyer_id);

                    $btn = " <a data-id='$row->id' class='seller_login ' title='Insurance Agency Login'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-log-in font-small-4'><path d='M15 3h4a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2h-4'></path><polyline points='10 17 15 12 10 7'></polyline><line x1='15' y1='12' x2='3' y2='12'></line></svg></a>";

					if (!$this->checkPlan($row->id)) {
						$btn.= "<a data-id='$row->id' class='ml-1 activateLicense' title='Activate License'><svg viewBox='0 0 24 24' width='20' height='20' stroke='currentColor' stroke-width='2' fill='none' stroke-linecap='round' stroke-linejoin='round' class='css-i6dzq1'><path d='M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2'></path><circle cx='8.5' cy='7' r='4'></circle><polyline points='17 11 19 13 23 9'></polyline></svg></a>";
					}
                    return $btn;
                })
                ->rawColumns(['action','status'])
                ->make(true);
            }

            $title = 'Seller';
			$plans = Plan::all();
            return view('seller.index',compact('title','plans'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('buyer');
        }
	}

	public function loginUser($user_id)
    {   
        if (isset($user_id) && $user_id > 0) {
            Auth::loginUsingId($user_id);
            return redirect('home');
        }
    }

	public function activateLicense(Request $request) {
		$user = User::where('id',$request->user_id)->update(['created_at'=>now()]);
		UserPlan::create($request->all());
		toastr()->success("License Activated Successfully");
        return redirect()->back();
	}

	public function webSolutions() {
		return view('web_solution');
	}

	public function sellerPlan() {
		$plans = Plan::all();
		$title = 'Plan';
		return view('seller.plan',compact('plans','title'));
	}

	public function requests(Request $request) {
		try {
            if ($request->ajax()) {
                $data = UserPlanRequest::join('plans','plans.plan_id','user_plan_requests.plan_id')
				->join('users','users.id','user_plan_requests.user_id')->get();
				// dd($data);

                return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('Status', function ($row){
					$id= $row->user_plan_request_id;
					$mode = $row->Status;
					if($mode == 'Accepted') {
						return $mode;
					} else {
						if (Auth::user()->user_type == 'SA') {
							return "<select class='form-control acceptRequest' data-id='$id' data-minimum-results-for-search='Infinity'>
							<option value='Pending' " . ($mode == 'Pending' ? 'selected' : '') . ">Pending</option>
							<option value='Accepted' " . ($mode == 'Accepted' ? 'selected' : '') . ">Accepted</option>
							<option value='Requested' " . ($mode == 'Requested' ? 'selected' : '') . ">Requested</option>
							<option value='Rejected' " . ($mode == 'Rejected' ? 'selected' : '') . ">Rejected</option>
						</select>"; 
						} else {
							return $mode;
						}
					}	
                })
				->addColumn('order_id', function ($row){
					$userPlanRequest = UserPlanRequest::where('user_id',$row->id)->first();
					if ($userPlanRequest != null) {
						return '240330SR'.$userPlanRequest->user_plan_request_id;
					} else {
						return null;
					}
				})
				->editColumn('created_at', function ($row){
						return $row->created_at->format('d-m-Y');
				})
                ->rawColumns(['action','Status'])
                ->make(true);
            }

            $title = 'Requests';
            return view('seller.request',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('buyer');
        }
	}

	public function submitContactUs(Request $request) {
		$input = $request->all();
		ContactUs::create($input);
		toastr()->success('Form Submitted Successfully');
		return redirect()->back();
	}

	public function Enquiry(Request $request) {
		try {
            if ($request->ajax()) {
                $data = ContactUs::all();

                return Datatables::of($data)
                ->addIndexColumn()
				->editColumn('created_at', function ($row){
						return $row->created_at->format('d-m-Y');
				})
                ->rawColumns(['action','Status'])
                ->make(true);
            }

            $title = 'Enquiry';
            return view('enquiry',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
			return redirect()->back();
        }
	}

	public function banner(Request $request) {
		try {
            if ($request->ajax()) {
                $data = Banner::all();

                return Datatables::of($data)
                ->addIndexColumn()
				->editColumn('image', function ($row){
					return "<img src='$row->image' style='width: 150px;height: 80px;'>";
				})
				->editColumn('created_at', function ($row){
						return $row->created_at->format('d-m-Y');
				})
				->addColumn('action', function ($row) {
                    $id  = encrypt($row->banner_id);

                    $btn = " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='Banner' title='Delete Invoice'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
				->rawColumns(['image','action'])
                ->make(true);
            }

            $title = 'Banner';
            return view('banner',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
			return redirect()->back();
        }
	}

	public function bannerStore(Request $request) {
		$imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('images'), $imageName);
		$input['image'] = env('APP_URL').'/public/images/'.$imageName;
		Banner::create($input);
		toastr()->success('Banner Created Successfully');
		return redirect()->back();
	}
}