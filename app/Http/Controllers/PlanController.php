<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use App\Models\User;
use App\Models\UserPlan;
use App\Models\UserPlanRequest;
use Illuminate\Http\Request;
use App\Traits\CommonTrait;
use DataTables;
use Illuminate\Support\Facades\Auth;

class PlanController extends Controller
{
    use CommonTrait;
    public function __construct()
    {
        $this->title = "Plans";
        $this->middleware('auth');
    }

    public function index(Request $request) {

        try {
            if ($request->ajax()) {
                $sellerId = $this->getSellerId();
                $data = Plan::query();
                
                $this->checkPlan(10);

                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) use ($request){
                    $id  = encrypt($row->plan_id);
                    
                    $btn = "
                    <a href='".url('/plans/edit/'.$id)."' class='item-edit text-dark' title='Edit Plan'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";
                    

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='Plan' title='Delete Plan'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->rawColumns(['action','status'])
                ->make(true);
            }

            $title = $this->title;
            return view('plan.index',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('plans');
        }
    }

    public function create() {
        $title = $this->title;
        return view('plan.create',compact('title'));
    }

    public function store(Request $request) {
        try {
            $input = $request->all();

            Plan::create($input);
            toastr()->success("Plan Created Successfully");
            return redirect('plans');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('plans/create');
        }
    }

    public function edit($planId) {
        try {
            $title = $this->title;
            $plan = Plan::find(decrypt($planId));
            return view('plan.edit',compact('title','plan'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('plans');
        }
    }

    public function update(Request $request) {
        try {
            $input = $request->all();

            $searchInput['plan_id'] = $input['plan_id'];

            Plan::updateOrcreate($searchInput, $input);
            toastr()->success("Plan Updated Successfully");
            return redirect('plans');

        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('plans');
        }
    }

    public function getData(Request $request) {
        try {
            $plan = Plan::find($request->id);
            $resultArr['data']    = $plan;
            echo json_encode($resultArr);
            exit;   
        } catch (\Exception $e) {
            $resultArr['title']   = 'Error';
            $resultArr['message'] = Config('messages.500');
            echo json_encode($resultArr);
            exit;   
        }
    }

    public function buyPlan(Request $request) {
        try {
            $input = $request->all();
            $input['user_id'] = Auth::id();
            $input['Status'] = 'Requested';
            UserPlanRequest::create($input);
            toastr()->success('Activation Requested Successfully');
            return redirect('home');
        } catch (\Throwable $th) {
            dd($th);
        }
    }

    public function updateRequest(Request $request) {
        try {
            $plan = UserPlanRequest::where('user_plan_request_id',$request->id)->update(['Status'=>$request->status]);

            if ($request->status == 'Accepted') {
                $plan = UserPlanRequest::find($request->id);
                $user = User::where('id',$plan->user_id)->update(['created_at'=>now()]);
                UserPlan::create(['user_id'=>$plan->user_id,'plan_id'=>$plan->plan_id]);
            }
           
            $resultArr['title']   = 'Success';
            $resultArr['message'] = 'Status Changed Successfully';
            echo json_encode($resultArr);
            exit;   
        } catch (\Exception $e) {
            dd($e);
            $resultArr['title']   = 'Error';
            $resultArr['message'] = Config('messages.500');
            echo json_encode($resultArr);
            exit;   
        }
    }

    
}
