<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\Customer;
use App\Models\Otp;
use App\Models\ShortenedUrl;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use App\Models\Seller;
use App\Traits\CommonTrait;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers,CommonTrait;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // Login
    public function showLoginForm(){
      $pageConfigs = [
          'bodyClass' => "bg-full-screen-image",
          'blankPage' => true
      ];

      return view('/auth/login', [
          'pageConfigs' => $pageConfigs
      ]);
    }

    public function login(Request $request)
    {
        // try {
            $this->validateLogin($request);

            // If the class is using the ThrottlesLogins trait, we can automatically throttle
            // the login attempts for this application. We'll key this by the username and
            // the IP address of the client making these requests into this application.
            if (method_exists($this, 'hasTooManyLoginAttempts') &&
                $this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);
    
                return $this->sendLockoutResponse($request);
            }
    
            if ($this->attemptLogin($request)) {
                if ($request->hasSession()) {
                    $request->session()->put('auth.password_confirmed_at', time());
                }
    
                $id = Auth::id();
                $user = User::where('id',$id)->first();
    
                if($user->user_status == "Inactive"){
                  $this->guard()->logout();
                  toastr()->error('Your account has been disabled by admin.');
                  return redirect('login');
                }
                
                if ($user->user_type == 'B') {
                    // if ($this->checkPlan($user->id)) { 
                        return redirect('home');
                    // } else {
                    //     toastr()->error('Your plan has been expired, please contact to admin.');
                    //     $this->guard()->logout();
                    //     return redirect('seller/login');
                    // }
                }
                
                return redirect('home');
            }
    
            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);
    
            return $this->sendFailedLoginResponse($request);
        // } catch (\Exception $e) {
        //     toastr()->error(Config('messages.500'));
        //     return redirect()->back();
        // }
       
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }

        return $request->wantsJson()
                    ? new JsonResponse([], 204)
                    : redirect()->route('home');
    }

    public function logout(Request $request)
    {   
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/seller/login');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        toastr()->error('Your credentials does not match with our records');
        return redirect('login');
    }

    // Login
    public function sellerLoginForm(){
        return view('/auth/seller_login');
    }

    public function loginWithOtp() {
        return view('auth.login_with_otp');
    }

    public function sellerRegister() {
        return view('auth.seller_register');
    }
}
