<?php

namespace App\Http\Controllers;

use App\Models\Buyer;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\Invoice;
use Illuminate\Support\Facades\Auth;

class BuyerController extends Controller
{
    use CommonTrait;
    public function __construct()
    {
        $this->middleware('auth');
        $this->title = "Buyer";
    }

    public function index(Request $request) {

        try {
            if ($request->ajax()) {
                $sellerId = $this->getSellerId();
                $data = Buyer::join('users','users.id','buyers.user_id')->where('seller_id',$sellerId);
                

                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('state', function ($row){
                    $buyer = Buyer::find($row->buyer_id);
                    $buyer->load('getState');
                    return $buyer->getState->name;
                })
                ->addColumn('city', function ($row){
                    $buyer = Buyer::find($row->buyer_id);
                    $buyer->load('getCity');
                    // dd($buyer);
                    return $buyer->getCity->name;
                })
                ->addColumn('action', function ($row){
                    $id  = encrypt($row->buyer_id);

                    $btn = "";

                    $btn .= "
                    <a href='".url('/buyer/edit/'.$id)."' class='item-edit text-dark' title='Edit Product'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";
                    

                    $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='Buyer' title='Delete Buyer'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->rawColumns(['action','status'])
                ->make(true);
            }

            $title = $this->title;
            return view('buyer.index',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('buyer');
        }
    }

    public function create() {
        $title = $this->title;

        $states = $this->getStates();
        return view('buyer.create',compact('title','states'));
    }

    public function store(Request $request) {
        try {
            $input = $request->all();

            // // Check whether same phone no is exist or not
            // $phoneData = $this->duplicateUserRecord($input, 'user_phone_no', 'User');

            // if ($phoneData->isNotEmpty()) {
            //     toastr()->error('User ' . Config('messages.phoneExist'));
            //     return redirect('buyer/create')->withInput();
            // }

            $uInput['user_fname'] = $input['user_fname'];
            $words = explode(" ", $input['user_fname']); // Split the string into words
            $initials = "";

            foreach ($words as $word) {
                $initials .= substr($word, 0, 1); // Append the first letter of each word
            }
            $uInput['email'] = $initials.$this->getSellerId().'@ekhatabook.com';
            $uInput['user_phone_no'] = $input['user_phone_no'];
            $uInput['password'] = Hash::make($input['password']);
            $uInput['user_type'] = 'Buyer';
            $user = User::create($uInput);

            $input['user_id'] = $user->id;
            $input['seller_id'] = $this->getSellerId();
            Buyer::create($input);
            toastr()->success("Buyer Created Successfully");
            return redirect('buyer');
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('buyer/create');
        }
    }

    public function edit($buyerId) {
        try {
            $title = $this->title;
            $buyer = Buyer::join('users','users.id','buyers.user_id')->where('buyer_id',decrypt($buyerId))->first();
            $states = $this->getStates();
            $cities = $this->getCities();
            return view('buyer.edit',compact('title','buyer','states','cities'));
        } catch (\Throwable $th) {
            toastr()->error(Config('messages.500'));
            return redirect('product');
        }
    }

    public function update(Request $request) {
        try {
            $input = $request->all();

            // // Check whether same phone no is exist or not
            // $phoneData = $this->duplicateUserRecord($input, 'user_phone_no', 'User', 'id');

            // if ($phoneData->isNotEmpty()) {
            //     toastr()->error('User ' . Config('messages.phoneExist'));
            //     return redirect()->back();
            // }

            $uInput['user_fname'] = $input['user_fname'];
            $uInput['user_phone_no'] = $input['user_phone_no'];
            $usearchInput['id'] = $input['id'];
            User::updateorcreate($usearchInput, $uInput);

            $searchInput['buyer_id'] = $input['buyer_id'];

            Buyer::updateOrcreate($searchInput, $input);
            toastr()->success("Buyer Updated Successfully");
            return redirect('buyer');

        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('buyer');
        }
    }

    public function getCreditDetails(Request $request) {
        try {
            $getCreditAmount = Invoice::select(DB::raw("MIN(invoice_products.price * invoice_products.quantity) as credit"),DB::raw("SUM(invoice_products.price * invoice_products.quantity) as totalValue"))->join('invoice_products','invoice_products.invoice_id','invoices.invoice_id')->where('buyer_id',decrypt($request->id))->get();

            $data['getCreditAmount'] = $getCreditAmount[0]->credit;
            $data['totalValue'] =  $getCreditAmount[0]->totalValue;

            $resultArr['title']   = 'Success';
            $resultArr['message'] = 'Data Retrieved Successfully';
            $resultArr['data']    = $data;
            echo json_encode($resultArr);
            exit;   
        } catch (\Exception $e) {
            dd($e);
            $resultArr['title']   = 'Error';
            $resultArr['message'] = Config('messages.500');
            echo json_encode($resultArr);
            exit;   
        }
    }

    public function buyerReport(Request $request) {
        try {
            $sellerId = $this->getSellerId();
            if ($request->ajax()) {
                $data = Invoice::join('buyers','buyers.buyer_id','invoices.buyer_id')  
                ->join('users','users.id','buyers.user_id')  
                ->join('states','states.state_id','buyers.state_id')->where('invoices.seller_id',$sellerId)
                ->where('invoice_type','Invoice');

                if($request->range != '') {

					if (strpos($request->range, " to ") !== false) {

						list($startDate, $endDate) = explode(" to ", $request->range);

						$data = $data->where(function($query) use ($startDate, $endDate) {
							$query->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$startDate])
								  ->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') <= ?", [$endDate]);
						});						
							
					} else {
						$data = $data->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$request->range]);
					}
				}

                if($request->buyer != '') {
                    $data = $data->where('invoices.buyer_id', $request->buyer);
                }

                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) use ($sellerId){
                    $id  = encrypt($row->buyer_id);

                    $profit = Invoice::select(DB::raw("SUM(invoices.profit) as profit"))->where('invoices.buyer_id',$row->buyer_id)->where('invoices.seller_id',$sellerId)->get();
                    // dump($profit);
                    
                    $btn = "";
                    // dd($profit[0]->profit);
                    if($row->payment_mode == "Credit") {
                        $btn.="<a class='item-edit text-dark viewCredit' data-id='$id' data-amount ='$row->grand_total' data-date='$row->date' title='View Credit'><svg viewBox='0 0 24 24' width='16' height='16' stroke='currentColor' stroke-width='2' fill='none' stroke-linecap='round' stroke-linejoin='round' class='css-i6dzq1'><path d='M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z'></path><circle cx='12' cy='12' r='3'></circle></svg></a>";
                    }

                    // $btn .= "
                    // <a href='".url('/buyer/edit/'.$id)."' class='item-edit text-dark' title='Edit Product'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit font-small-4'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>";
                    

                    // $btn .= " <a class='delete-record delete item-edit text-danger' data-id='$id' data-model='Buyer' title='Delete Buyer'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-trash-2 font-small-4'><path d='M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2'></path><line x1='10' y1='11' x2='10' y2='17'></line><line x1='14' y1='11' x2='14' y2='17'></line></svg></a>";
                 
                    return $btn;
                })
                ->editColumn('payment_mode', function ($row) {
                    $id = $row->invoice_id;
                    $mode = $row->payment_mode;
                    if ($mode == 'Credit') {
                        return "<select class='form-control updateMode' data-id='$id' data-minimum-results-for-search='Infinity'>
                            <option value='Dacc' " . ($mode == 'Dacc' ? 'selected' : '') . ">Dacc</option>
                            <option value='Paid' " . ($mode == 'Paid' ? 'selected' : '') . ">Paid</option>
                            <option value='Credit' " . ($mode == 'Credit' ? 'selected' : '') . ">Credit</option>
                        </select>";  
                    } else {
                        return $mode;
                    }
                })
                ->rawColumns(['action','status','payment_mode'])
                ->make(true);
            }

            $title = $this->title;
            $buyers = $this->getBuyers($sellerId);
            return view('buyer.buyer-report',compact('title','buyers'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('buyer');
        }
    }

    public function totalProfit(Request $request) {
        try{
            $sellerId = $this->getSellerId();
            $profit = Invoice::select(DB::raw("SUM(invoices.profit) as profit"))->where('invoices.buyer_id',$request->buyer)->where('invoices.seller_id',$sellerId)->get();
            $data['totalProfit'] = $profit[0]->profit;
            $resultArr['title']   = 'Success';
            $resultArr['message'] = 'Data Deleted Successfully';
            $resultArr['data'] = $data;
            echo json_encode($resultArr);
            exit;   
        } catch (\Exception $e) {
            $resultArr['title']   = 'Error';
            $resultArr['message'] = Config('messages.500');
            echo json_encode($resultArr);
            exit;   
        }
    }

    public function updateMode(Request $request) {
        try{
            $invoice = Invoice::where('invoice_id',$request->id)->update(['payment_mode'=>$request->mode]);
            $resultArr['title'] = 'Success';
            $resultArr['message'] = 'Payment Mode changed successfully.';
            echo json_encode($resultArr);
            exit;
        }catch(\Exception $e) {
            $resultArr['title'] = 'Error';
            $resultArr['message'] = 'Something went wrong.';
            echo json_encode($resultArr);
            exit;
        }
    }

    public function buyerBills(Request $request) {
        try {
            $buyerId = Buyer::where('user_id',Auth::id())->value('buyer_id');
            if ($request->ajax()) {
                $data = Invoice::join('buyers','buyers.buyer_id','invoices.buyer_id')  
                ->join('users','users.id','buyers.user_id')  
                ->join('states','states.state_id','buyers.state_id')->where('invoices.buyer_id',$buyerId)
                ->where('invoice_type','Invoice');

                if($request->range != '') {

					if (strpos($request->range, " to ") !== false) {

						list($startDate, $endDate) = explode(" to ", $request->range);

						$data = $data->where(function($query) use ($startDate, $endDate) {
							$query->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$startDate])
								  ->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') <= ?", [$endDate]);
						});						
							
					} else {
						$data = $data->whereRaw("DATE_FORMAT(STR_TO_DATE(invoices.date, '%Y-%m-%d'), '%m-%d-%Y') >= ?", [$request->range]);
					}
				}

                return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row){
                    $id  = encrypt($row->buyer_id);
                    
                    $btn = "";
                    if($row->payment_mode == "Credit") {
                        $btn.="<a class='item-edit text-dark viewCredit' data-id='$id' data-amount ='$row->grand_total' data-date='$row->date' title='View Credit'><svg viewBox='0 0 24 24' width='16' height='16' stroke='currentColor' stroke-width='2' fill='none' stroke-linecap='round' stroke-linejoin='round' class='css-i6dzq1'><path d='M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z'></path><circle cx='12' cy='12' r='3'></circle></svg></a>";
                    }      
                    
                    $btn .= "<a href='".url('/invoice/download/'.encrypt($row->invoice_id))."' class='item-edit text-dark' title='Download Invoice' style='margin-left:3px;'><svg viewBox='0 0 24 24' width='16' height='16' stroke='currentColor' stroke-width='2' fill='none' stroke-linecap='round' stroke-linejoin='round' class='css-i6dzq1'><path d='M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4'></path><polyline points='7 10 12 15 17 10'></polyline><line x1='12' y1='15' x2='12' y2='3'></line></svg></a>";

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
            }

            $title = $this->title;
            return view('buyer.sell',compact('title'));
        } catch (\Exception $e) {
            toastr()->error(Config('messages.500'));
            return redirect('buyer');
        }
    }

    public function checkCredit(Request $request) {
        try{
            $buyer = Buyer::find($request->buyer);
            $invoice = Invoice::select(DB::raw("SUM(profit) as profit"))->where('buyer_id',$request->buyer)->get();
        
            if ($invoice[0]->profit >= $buyer->target) {
                $eligible = true;
            } else {
                $eligible = false;
            }
            $resultArr['title'] = 'Success';
            $resultArr['message'] = 'buyer details checked successfully.';
            $resultArr['eligible'] = $eligible;
            echo json_encode($resultArr);
            exit;
        }catch(\Exception $e) {
            dd($e);
            $resultArr['title'] = 'Error';
            $resultArr['message'] = 'Something went wrong.';
            echo json_encode($resultArr);
            exit;
        }
    }
}
