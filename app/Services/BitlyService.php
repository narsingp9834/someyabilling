<?php 
    namespace App\Services;

    use GuzzleHttp\Client;
    
    class BitlyService
    {
        protected $client;
        protected $accessToken;
    
        public function __construct()
        {
            $this->client = new Client([
                'base_uri' => 'https://api-ssl.bitly.com/v4/shorten',
            ]);
    
            $this->accessToken = Config('constants.bitly_access_token');
        }
    
        public function shortenUrl($longUrl)
        {
            $response = $this->client->post('shorten', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->accessToken,
                ],
                'json' => [
                    'long_url' => $longUrl,
                ],
            ]);
    
            $body = json_decode($response->getBody(), true);
    
            return $body['id'] ?? null;
        }
    }    

?>