<?php

namespace App\Exports;

use App\Models\Supplier;
use App\Traits\CommonTrait;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Helpers\Helper;

class PurchaseExport implements FromCollection, WithHeadings
{
    use CommonTrait;
    public function __construct()
    {
        $this->heading = ['Date', 'Invoice No', 'Supplier Name', 'Phone No', 'GST No', 'Product', 'HSN Code', 'Price', 'Quantity', 'IGST', 'CGST', 'SGST','Total', 'Payment Mode', 'State'];

    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {   

            $data = Supplier::select('date','invoice_no','supplier_name','phone_no','gst_no','product','hsn_code','price','quantity','igst','cgst','sgst','Total', 'payment_mode','states.name')
            ->join('states','states.state_id','suppliers.state_id')
            ->where('seller_id',$this->getSellerId())
            ->get();
            

            $data->transform(function ($item){

                $generatorData1 = [
                    $item->date,
                    $item->invoice_no,
                    $item->supplier_name,
                    $item->phone_no, 
                    $item->gst_no, 
                    $item->product,
                    $item->hsn_code,
                    $item->price, 
                    $item->quantity,
                    Helper::getTaxAmount(($item->price * $item->quantity), $item->igst ?? 0) . '(' . ($item->igst ?? 0) . '%)',
                    Helper::getTaxAmount(($item->price * $item->quantity), $item->cgst ?? 0) . '(' . ($item->cgst ?? 0) . '%)',
                    Helper::getTaxAmount(($item->price * $item->quantity), $item->sgst ?? 0) . '(' . ($item->sgst ?? 0) . '%)',
                    $item->Total,
                    $item->payment_mode,
                    $item->name
                ];

                return $generatorData1;
            });

            return $data;

            return $data;
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return $this->heading;
        
    }
}
