$( document ).ready(function() {    
    // Datatable
    if (document.getElementById("table_address_type")) {
        var table = $('#table_address_type').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url : base_url + "/service/list",
                data: function ( data ) {
                    data.timeZone   = jQuery("input[name='timeZone']").val();
                    data.status     = jQuery('#status').val();
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                {data: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'name', name: 'name'},
                {data: 'cost', name: 'cost', orderable: true, searchable: false},
                {data: 'cancellation_charge', name: 'cancellation_charge', orderable: true, searchable: false},
                {data: 'labor_cost', name: 'labor_cost', orderable: true, searchable: false},
                {data: 'created_at', searchable: false},
                {data: 'specialization_status', orderable: false},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
        });

        // Filter data status wise
        jQuery('#status').on('change', function(){
            table.draw();
        });
    }
});
$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("require_field", function(value, element) {
            if(value.trim() ==''){
                return false;
            }
            return true;
        }, "This field is required.");

        jQuery.validator.addMethod('ckrequired', function (value, element, params) {
            var idname = jQuery(element).attr('id');
            var messageLength =  jQuery.trim ( noteEditor.getData() );
            return !params  || messageLength.length !== 0;
        }, "This field is required");

        // jQuery.validator.addMethod('cklength', function (value, element, params) {
        //     var messageLength = jQuery.trim(noteEditor.getData());
        //     console.log(messageLength.length);
        //     return !params || messageLength.length >= 1000;
        // }, "Please enter at least 1000 characters.");
    
        jqForm.validate({
            ignore: [],
             rules: {
                description: {
                    ckrequired : true,
                    // cklength: true,
                    // maxlength: 10,
                },
             },
             errorElement: 'span',
             errorPlacement: function (error, element) {
                 element.closest('.form-group').append(error);
             },
             submitHandler: function (form) {
                 $('#loader').show(); 
                 form.submit();
             }
        });

        // $('#service_submit').on('click', function () {
        // if ($('#jquery-val-form').valid()) {
            
        //     loader('show');
        // }
        // });

        // $(document).on('submit','#jquery-val-form',function(){
        //     return false;
        //  });
        
        // jqForm.validate({
        //     rules: {
        //         'address_type_name': {
        //             required: true,
        //             maxlength:50,
        //             require_field: true,
        //         },
        //     },
        //     messages: {
        //     },
        //     submitHandler: function (form) {
        //         $('#loader').show(); 
        //         form.submit();
        //     }
        // });
    }
});