$( document ).ready(function() {      
    if (document.getElementById("table_gstr1")) {
        var table = $('#table_gstr1').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/gstr1",
                data: function (data) {
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'created_at', name: 'created_at'},
                { data: 'invoice_no', name: 'invoice_no' },
                { data: 'party_name', name: 'party_name' },
                { data: 'state', name:'state'},
                { data: 'gst_no', name:'gst_no'},
                { data: 'total_amount', name: 'total_amount'},
                { data: 'total_pieces', name: 'total_pieces'},
            ],
        });
    }

});

