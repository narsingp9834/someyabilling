
$( document ).ready(function() {      
    if (document.getElementById("table_quotation")) {
        var table = $('#table_quotation').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            ajax: {
                url: base_url + "/quotation",
                data: function (data) {
                }
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'user_fname', name: 'user_fname' },
                { data: 'invoice_no', name: 'invoice_no'},
                { data: 'grand_total', name: 'grand_total'},
                { data: 'to_invoice', name: 'to_invoice'},
                { data: 'action', name: 'action', orderable: false, searchable: false },
            ],
        });
    }
});
