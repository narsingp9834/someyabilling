$(document).ready(function () {
    if (document.getElementById("appointments")) {
        
        var table = $('#appointments').DataTable({
            processing: true,
            serverSide: true,
            order: [0, 'DESC'],
            dom:
                '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            columnDefs : [
                { targets: 3, visible: document.getElementById('role').value != 'C' },
                { targets: 5, visible: document.getElementById('role').value != 'C' },
                { targets: 4, visible: (document.getElementById('role').value != 'T') && (document.getElementById('role').value != 'C') },
                { targets: 6, visible: (document.getElementById('role').value != 'T') && (document.getElementById('role').value != 'C')},
            ],
            ajax: {
                url: base_url + "/appointment/list/",
                data: function (data) {
                    data.timeZone = jQuery("input[name='timeZone']").val();
                    data.technician = jQuery('#technician').val();
                    data.customer = jQuery('#customer').val();
                    data.specialization = jQuery('#specialization').val();
                    data.status = jQuery('#status').val();
                    data.customerType = jQuery('#customerType').val();
                    data.zone  = $('#zone').val();
                    data.slot_date = $('#slot_date').val();
                    data.slot_time = $('#slot_time').val();
                }
            },
            "columns": [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'request_type', name: 'specializations.name',orderable: true, searchable: true},
                { data: 'slot_time', searchable: false },
                { data: 'customer_name', name:'customer_name'},
                { data: 'technician_name', name:'technician_name'},
                { data: 'city' ,name:'cities.name' },
                { data: 'zone_name',name:'zones.zone_name' },
                { data: 'appointment_status',orderable: false, searchable: false},
                { data: 'action',orderable: false, searchable: false },
            ],
            drawCallback: function(dt) {
                $('.aselect2').select2({ tags: true, width: "6em" });
            },
        });

        jQuery('#technician').on('change', function () {
            table.draw();
        });

        jQuery('#customer').on('change', function () {
            table.draw();
        });

        jQuery('#specialization').on('change', function () {
            table.draw();
        });
        
        jQuery('#status').on('change', function () {
            table.draw();
        });

        jQuery('#zone').on('change', function () {
            table.draw();
        });

        jQuery('#slot_date').on('change', function () {
            table.draw();
        });

        jQuery('#slot_time').on('change', function () {
            table.draw();
        });
        if($("#customerType").val() != '' && $("#customerType").val() != undefined){
            getCustomers();
        }

        $("#customerType").change(function () {
            getCustomers();
        });
        
        function getCustomers(){
            var endpoint = base_url + '/common/getCustomersByType';
            var selected_type = $("#customerType").val();
            var token = jQuery("input[name='_token']").val();
            $('#customer').html('<option value="">Select Customer</option>');
            $.ajax({
                url: endpoint,
                method: 'POST',
                data: {
                    '_token': token,
                    'selected_type': selected_type,
                },
                dataType: "json",
                success: function (data) {
                    var len = data.length;
                    for (var i = 0; i < len; i++) {
                        var selected = '';
                        if (($('#customer_id').val() == data[i].customer_id)) { 
                            selected = ' selected '; 
                        }
                        if ((data[i].user_fname != null) && (data[i].user_lname != null)) {
                            $('#customer').append('<option value="' + data[i].customer_id + '" '+ selected +'>' + data[i].user_fname + ' ' + data[i].user_lname + '</option>');   
                        } else {
                            $('#customer').append('<option value="' + data[i].customer_id + '" '+ selected +'>' + data[i].company_name + '</option>');  
                        }
                    }
                    $('#customer').trigger('change');
                }
            });
            table.draw();
        }

    }

    $("#jquery-val-form input:radio").change(function () {
        var label = $(this).next('label').text();
        if (label == "Other - My error code is not listed") {
            bootbox.confirm({
                title: "Question",
                message: "Please call our office to schedule",
                buttons: {
                    confirm: {
                        label: 'OK',
                        className: 'btn-danger'
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-secondary'
                    }
                },
                callback: function (result) {
                    if (result == true) {
                        window.location.href=base_url + "/appointment/list"
                    }
                }
            });
        }
    });

    $(document).on('click','.act_timeslot',function(){
        var slotId = $("#apt_slot").val();
        if(slotId!=""){
            $("#"+slotId).removeClass('selected_timeslot');
        }
        $("#apt_slot").val(this.id);
        $(this).addClass('selected_timeslot');
    });

    $(document).on('click','#add-appointment', function () {
        var slot_date = $("#apt_date").val();
        var slot_id = $("#apt_slot").val();
        var apt_id = $("#apt_id").val(); 

        if(slot_date == ""){
            bootbox.alert({
                message: "Please select date.",
            });
        }

        if(slot_id == ""){
            bootbox.alert({
                message: "Please choose time slot.",
            });
            return;
        }
        $('#loader').show();
        var token = $("input[name='_token']").val();
        $.ajax({
            url: base_url + "/appointment/saveData",
            type: 'POST',
            data: {
                '_token': token,
                'slot_date': slot_date,
                'slot_id': slot_id,
                'temp_id': $("#temp_id").val(),
                'apt_id' : apt_id,
                'smsNo'  : $('#smsNo').val()
            },
            success: function (data) {
                $('#loader').hide();
                if(data.status == "success"){
                    bootbox.alert({
                        message: data.msg,
                        callback: function () {
                            window.location.href=base_url + "/appointment/list"
                        }
                    });
                }else{
                    if(data.type == "reschedule"){
                        bootbox.alert({
                            message: data.msg,
                            callback: function () {
                                window.location.href=base_url + "/appointment/list"
                            }
                        });
                    }else{
                        bootbox.alert({
                            message: data.msg,
                        });
                    }
                }
            },
            error: function (jXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });
    });
});

$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {
        jQuery.validator.addMethod("require_field", function (value, element) {
            if (value.trim() == '') {
                return false;
            }
            return true;
        }, "This field is required.");

        jqForm.validate({
            rules: {
            },
            messages: {
            }
        });
    }
    
    $('#jquery-val-form').submit(function (e) {
        e.preventDefault();
        if(jqForm.valid()){
            if($("#service_id").val() == 3){
                var message = $("#service_message").val(); 
                bootbox.confirm({
                    title: "Schedule a Request",
                    message: message,
                    buttons: {
                        confirm: {
                            label: 'I Accept These Charges',
                            className: 'btn-danger'
                        },
                        cancel: {
                            label: 'I decline',
                            className: 'btn-secondary'
                        }
                    },
                    callback: function (result) {
                        if (result == true) {
                            $('#loader').show();
                            saveData();
                        }
                    }
                });
            }
            else if($("#service_id").val() == 2){
                var message = $("#service_message").val(); 
                var v = $('input[type=radio].qradio:checked');
                var count = 0;
                $(v).each(function(i){
                    var label = $(this).next('label').text();
                    if (label == "Other - My error code is not listed" || label == "Other - My error code is not listed.") {
                        count = count + 1;
                        bootbox.confirm({
                            title: "Question",
                            message: "Please call our office to schedule",
                            buttons: {
                                confirm: {
                                    label: 'OK',
                                    className: 'btn-danger'
                                },
                                cancel: {
                                    label: 'Cancel',
                                    className: 'btn-secondary'
                                }
                            },
                            callback: function (result) {
                                if (result == true) {
                                    window.location.href=base_url + "/appointment/list"
                                }
                            }
                        });
                    }
                });
                if(count == 0){
                    $('#loader').show();
                    saveData();
                }
            }
            else{
                $('#loader').show();
                saveData();
            }
        }
    });
});

$(function () {
    var jqForm1 = $('#jquery-val-form1');
    if (jqForm1.length) {
        jQuery.validator.addMethod("require_field", function (value, element) {
            if (value.trim() == '') {
                return false;
            }
            return true;
        }, "This field is required.");

        jqForm1.validate({
            rules: {
            },
            messages: {
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                element.closest('.form-group').append(error);
            },
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});

// $(function () {
//     var queForm = $('#question-form');
//     if (queForm.length) {
//         jQuery.validator.addMethod("require_field", function (value, element) {
//             if (value.trim() == '') {
//                 return false;
//             }
//             return true;
//         }, "This field is required.");

//         queForm.validate({
//             rules: {
//             },
//             messages: {
//             },
//             submitHandler: function (form) {
//                 $('#loader').show(); 
//                 form.submit();
//             }
//         });
//     }
// });
// $(function () {
//     var edit_form = $('#appointment-edit-form');
//     if (edit_form.length) {

//         edit_form.validate({
//             rules: {
//             },
//             messages: {
//             },
//             errorElement: 'span',
//             errorPlacement: function (error, element) {
//                 element.closest('.form-group').append(error);
//             },
//             submitHandler: function (form) {
//                 $('#loader').show(); 
//                 form.submit();
//             },
//         });
//     }
// });

function saveData(){
    $.ajax({
        url: base_url + "/appointment/save",
        type: $('#jquery-val-form').attr('method'),
        data: $('#jquery-val-form').serialize(),
        success: function (data) {
            $('#loader').hide();
            if(data.service_id == 4 || data.service_id == 3){
                bootbox.alert({
                    message: data.msg,
                    callback: function () {
                        window.location.href=base_url + "/appointment/list"
                    }
                });
            }else{
                window.location.href=base_url + "/appointment/choose-slot/"+$("#temp_id").val();
            }
        },
        error: function (jXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}


jQuery('#technician_id').on('change', function () {
    if($('#technician_id').val() != '' && $('#technician_id').val() != undefined){
        getTechnicianData();
    }else{
        $('#technician_email').val('');
        $('#technician_phone').val('');
    }
});


function getTechnicianData(){
    var technician_id = $('#technician_id').val();
    var token = jQuery("input[name='_token']").val();
    var slot_id = $("#slot_id").val();
    var slot_date = $("#slot_date").val();
    $.ajax({
        type: 'POST',
        dataType: "JSON",
        url: base_url+'/appointment/get-technician',
        data: {
            _token : token,
            technician_id     : technician_id,
            slot_id: slot_id,
            slot_date: slot_date
        },
        success: function(data) { 
            // success
            if(data.modalMsg!=""){
                bootbox.alert({
                    message: data.modalMsg,
                });
            }
            
            var status = data.status;
            var msg = data.msg;
            
            if(status == 'success'){
                var details = data.details;
                if(details != null){
                    $('#technician_email').val(details.email);
                    $('#technician_phone').val('+'+details.user_country_code + ' '+details.user_phone_no);
                }                    
            }else{
                toastr.error(msg);
            }
        },
    });
}


$('.slot_date').flatpickr({
    dateFormat: "m-d-Y",
    disableMobile: "true"
});
$('.slot_date').prop('readonly', false);