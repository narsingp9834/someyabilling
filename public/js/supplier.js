
// import monthSelectPlugin from "flatpickr/dist/plugins/monthSelect";  
  if (document.getElementById("table_supplier")) {
    var table = $('#table_supplier').DataTable({
        processing: true,
        serverSide: true,
        order: [0, 'DESC'],
        dom:
            '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-3"l><"row col-sm-12  col-md-5 customDropDown"><"col-sm-12 col-md-4"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        ajax: {
            url: base_url + "/supplier/list",
            data: function (data) {
                data.payment_mode     = jQuery("#payment_mode").val();
                data.range = jQuery('#range').val();
                data.month = jQuery('#month').val();
                data.year = jQuery('#year').val();
            },
            // success: function(response) {
            //     // Access DataTable data
            //     var dataTableData = response.data;
        
            //     // Access additional data
            //     var totalInvoices = response.totalInvoices;
            //     var totalPaid = response.totalPaid;
            //     var totalUnPaid = response.totalUnPaid;
            //     var totalPurchade = response.totalPurchade;
        
            //     // Use the data as needed
            //     console.log(totalPaid);
            // }
        },
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "columns": [
            { data: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'supplier_name', name: 'supplier_name' },
            { data: 'phone_no', name: 'phone_no'},
            { data: 'invoice_no', name: 'invoice_no'},
            { data: 'hsn_code', name: 'hsn_code'},
            { data: 'total', name: 'total'},
            { data: 'payment_mode', name: 'payment_mode'},
            { data: 'name', name: 'name'},
            { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
        // "drawCallback": function(settings) {  
        //     // $('#totalInvoice').html(this.api().rows().count());
        //     // Accessing the data of the second column
        //     var columnData = this.api().column(1).data();
        //     columnData.each(function(value) {
        //         console.log(value);
        //     });
        // }
    });
}

 jQuery("#payment_mode").on("change", function () {
        table.draw();
    });

    jQuery('#range').on('change', function () {
        table.draw();
        updateTotalCount();
    });

    
    jQuery('#month').on('change', function () {
        console.log("Month changed");
        updateTotalCount();
        table.draw();
    });

    jQuery('#year').on('change', function () {
        table.draw();
        updateTotalCount();
    });

function updateTotalCount() {

    var endpoint = base_url + '/supplier/filer-data';
    var token = jQuery("input[name='_token']").val();
    $.ajax({
        url: endpoint,
        method: 'POST',
        data: {
            '_token': token,
            'month': $('#month').val(),
            'range':$('#range').val(),
            'year' : $('#year').val()
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            $('#totalInvoice').html(data.data.totalInvoices);
            $('#totalPaid').html(data.data.totalPaid);
            $('#totalUnPaid').html(data.data.totalUnPaid);
            $('#totalPurchade').html(data.data.totalPurchade);
        }
    })
}


$(function () {
    var jqForm = $('#jquery-val-form');
    if (jqForm.length) {

        jqForm.validate({
            submitHandler: function (form) {
                $('#loader').show(); 
                form.submit();
            }
        });
    }
});

var rangePicker = $("#range").flatpickr({
    mode: "range",
    dateFormat: "m-d-Y"
});   

$("#year").flatpickr({
            dateFormat: "Y", // Display only year in the input field
            altFormat: "Y", // Display only year in the calendar dropdown
            altInput: true, // Enable alternate input
            mode: "single",
});