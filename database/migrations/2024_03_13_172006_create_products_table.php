<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('product_id');
            $table->bigInteger('seller_id')->nullable();
            $table->string('product_name')->nullable();
            $table->bigInteger('unit_id')->nullable();
            $table->bigInteger('hsn_id')->nullable();
            $table->bigInteger('max_quantity')->nullable();
            $table->bigInteger('min_quantity')->nullable();
            $table->bigInteger('purchase_price')->nullable();
            $table->bigInteger('sale_price')->nullable();
            $table->enum('status',['Active', 'Inactive'])->default('Active');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
