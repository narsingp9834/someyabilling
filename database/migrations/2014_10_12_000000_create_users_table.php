<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('user_fname')->nullable();
            $table->string('user_lname')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('user_type',['SA','A','B']);
            $table->enum('user_status',['Active','Inactive'])->default('Active');
            $table->dateTime('user_last_login')->nullable();
            $table->string('user_country_code',5)->nullable();
            $table->string('user_shortname',5)->nullable();
            $table->string('user_phone_no',50)->unique()->nullable();
            $table->string('user_profile_image')->nullable();
            $table->rememberToken();
            $table->bigInteger('added_by_user_id')->nullable();
            $table->bigInteger('updated_by_user_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
