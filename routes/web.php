<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CommonController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\BuyerController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ChalanController;
use App\Http\Controllers\CMSController;
use App\Http\Controllers\GSTController;
use App\Http\Controllers\HSNController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QuotationController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\UnitController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Main Page Route
// Route::group(['middleware' => ['httpsProtocol']], function () {
    Route::get('/not-authorized',       [CommonController::class, 'notAuthorized']);
    Route::get('/register',             [RegisterController::class, 'showRegistrationForm'])->name('register');
    Route::post('/register-store',      [RegisterController::class, 'store']);
    // Route::get('appointment/confirm/{id}',               [AppointmentController::class, 'confirmAppointment']);
    // Auth routes
    Route::get('/', [LoginController::class, 'showLoginForm']);
    Auth::routes(['verify' => true]);
    Route::get('logout', [LoginController::class, 'logout']);
    // Dashboard routes
    Route::get('/home', [DashboardController::class, 'dashboard'])->name('home');
    Route::post('/checkEmail',      [CommonController::class, 'checkEmail']);

    //Profile 
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', [ProfileController::class, 'editProfile']);
        Route::post('/update', [ProfileController::class, 'update']);
    });

    // Common routes
    Route::group(['prefix' => 'common'], function () {
        Route::post('/delete',           [CommonController::class, 'destroyRecord']);
        Route::post('/status',           [CommonController::class, 'updateStatus']);
        Route::post('/getCityById',      [CommonController::class, 'getCities']);
    });

    Route::group(['prefix' => 'supplier'], function () {
        Route::get('/list/{purchase?}',     [SupplierController::class, 'index']);
        Route::get('/create',   [SupplierController::class, 'create']);
        Route::post('/save',    [SupplierController::class, 'store']);
        Route::get('/edit/{id}', [SupplierController::class, 'edit']);
        Route::post('/update',  [SupplierController::class, 'update']);
        Route::get('/view/{id}', [SupplierController::class, 'view']);
        Route::get('/export', [SupplierController::class, 'export']);
        Route::post('/filer-data',  [SupplierController::class, 'filerData']);
    });

    Route::group(['prefix' => 'units'], function () {
        Route::get('/',     [UnitController::class, 'index']);
        Route::get('/create',   [UnitController::class, 'create']);
        Route::post('/save',    [UnitController::class, 'store']);
        Route::get('/edit/{id}', [UnitController::class, 'edit']);
        Route::post('/update',  [UnitController::class, 'update']);
    });

    Route::group(['prefix' => 'hsn'], function () {
        Route::get('/',     [HSNController::class, 'index']);
        Route::get('/create',   [HSNController::class, 'create']);
        Route::post('/save',    [HSNController::class, 'store']);
        Route::get('/edit/{id}', [HSNController::class, 'edit']);
        Route::post('/update',  [HSNController::class, 'update']);
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('/',     [ProductController::class, 'index']);
        Route::get('/create',   [ProductController::class, 'create']);
        Route::post('/save',    [ProductController::class, 'store']);
        Route::get('/edit/{id}', [ProductController::class, 'edit']);
        Route::post('/update',  [ProductController::class, 'update']);
        Route::post('/fetch-data',  [ProductController::class, 'fetchData']);
    });

    Route::group(['prefix' => 'buyer'], function () {
        Route::get('/',     [BuyerController::class, 'index']);
        Route::get('/create',   [BuyerController::class, 'create']);
        Route::post('/save',    [BuyerController::class, 'store']);
        Route::get('/edit/{id}', [BuyerController::class, 'edit']);
        Route::post('/update',  [BuyerController::class, 'update']);
        Route::post('/getCreditDetails',  [BuyerController::class, 'getCreditDetails']);
        Route::post('/checkCredit',  [BuyerController::class, 'checkCredit']);
    });

    Route::group(['prefix' => 'invoice'], function () {
        Route::get('/list/{sale?}',     [InvoiceController::class, 'index']);
        Route::get('/create',   [InvoiceController::class, 'create']);
        Route::post('/save',    [InvoiceController::class, 'store']);
        Route::get('/edit/{id}', [InvoiceController::class, 'edit']);
        Route::post('/update',  [InvoiceController::class, 'update']);
        Route::post('/gst-hsn',  [InvoiceController::class, 'gstHSN']);
        Route::post('/deleteProduct',  [InvoiceController::class, 'deleteProduct']);
        Route::get('/download/{id}', [InvoiceController::class, 'download']);
        Route::post('/makeInvoice',  [InvoiceController::class, 'makeInvoice']);
        Route::get('/export', [InvoiceController::class, 'export']);
        Route::post('/filter-data', [InvoiceController::class, 'filterData']);
    });

    Route::group(['prefix' => 'quotation'], function () {
        Route::get('/',     [QuotationController::class, 'index']);
        Route::get('/create',   [QuotationController::class, 'create']);
        Route::post('/save',    [QuotationController::class, 'store']);
        Route::get('/edit/{id}', [QuotationController::class, 'edit']);
        Route::post('/update',  [QuotationController::class, 'update']);
    });

    Route::group(['prefix' => 'chalan'], function () {
        Route::get('/',     [ChalanController::class, 'index']);
        Route::get('/create',   [ChalanController::class, 'create']);
        Route::post('/save',    [ChalanController::class, 'store']);
        Route::get('/edit/{id}', [ChalanController::class, 'edit']);
        Route::post('/update',  [ChalanController::class, 'update']);
    });

    Route::get('/gstr1',     [GSTController::class, 'gstr1']);
    Route::get('/gstr1/export',     [GSTController::class, 'gstr1Export']);
    Route::get('/gstr3',     [GSTController::class, 'gstr3']);
    Route::post('/getGst3Stats',     [GSTController::class, 'getGst3Stats']);

    Route::group(['prefix' => 'staff'], function () {
        Route::get('/',     [StaffController::class, 'index']);
        Route::get('/create',   [StaffController::class, 'create']);
        Route::post('/save',    [StaffController::class, 'store']);
        Route::get('/edit/{id}', [StaffController::class, 'edit']);
        Route::post('/update',  [StaffController::class, 'update']);
    });

    Route::group(['prefix' => 'plans'], function () {
        Route::get('/',     [PlanController::class, 'index']);
        Route::get('/create',   [PlanController::class, 'create']);
        Route::post('/save',    [PlanController::class, 'store']);
        Route::get('/edit/{id}', [PlanController::class, 'edit']);
        Route::post('/update',  [PlanController::class, 'update']);
    });

    Route::post('/activate-license',  [DashboardController::class, 'activateLicense']);
    Route::get('/web-solutions', [DashboardController::class, 'webSolutions']);
    Route::get('/buyer-report', [BuyerController::class, 'buyerReport']);
    Route::post('/buyer/total-profit',  [BuyerController::class, 'totalProfit']);
    Route::post('/buyer/updateMode',  [BuyerController::class, 'updateMode']);
    Route::get('/buyer/bills', [BuyerController::class, 'buyerBills']);
    Route::get('/seller-plan', [DashboardController::class, 'sellerPlan']);
    Route::post('/plan/getData', [PlanController::class, 'getData']);
    Route::post('/buyPlan', [PlanController::class, 'buyPlan']);
    Route::get('/requests', [DashboardController::class, 'requests']);
    Route::post('/updateRequest', [PlanController::class, 'updateRequest']);
    Route::get('/contact-us', function(){
        return view('contact_us');
     });
    Route::post('/submit-contact-us', [DashboardController::class, 'submitContactUs']);  
    Route::get('/enquiry', [DashboardController::class, 'Enquiry']);
    
    Route::group(['prefix' => 'banner'], function () {
        Route::get('/',     [DashboardController::class, 'banner']);
        Route::post('/store',    [DashboardController::class, 'bannerStore']);
    });

    Route::group(['prefix' => 'cms-product'], function () {
        Route::get('/',     [CMSController::class, 'productIndex']);
        Route::get('/create',     [CMSController::class, 'productCreate']);
        Route::post('/store',    [CMSController::class, 'productStore']);
        Route::get('/edit/{id}', [CMSController::class, 'productEdit']);
        Route::post('/update',  [CMSController::class, 'productUpdate']);
        Route::get('/view/{id}', [CMSController::class, 'productView']);
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('/',     [CategoryController::class, 'index']);
        Route::get('/create',     [CategoryController::class, 'create']);
        Route::post('/store',    [CategoryController::class, 'store']);
        Route::get('/edit/{id}', [CategoryController::class, 'edit']);
        Route::post('/update',  [CategoryController::class, 'update']);
        Route::get('/products/{id}', [CategoryController::class, 'products']);
    });

     //Change Password 
     Route::group(['prefix' => 'change-password'], function () {
        Route::get('/', [ProfileController::class, 'changePassword']);
        Route::post('/update', [ProfileController::class, 'updatePassword']);
    });

    Route::get('/t_c', function(){
        return view('terms_conditions');
     });

     Route::get('/disclaimers', function(){
        return view('disclaimers');
     }); 
     
     Route::get('/privacy_policy', function(){
        return view('privacy_policy');
     }); 
     

// });


